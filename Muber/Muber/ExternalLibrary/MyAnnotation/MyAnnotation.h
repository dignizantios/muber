//
//  MyAnnotation.h
//  MDLHR
//
//  Created by Bhavesh virani on 21/10/14.
//  Copyright (c) 2014 Dignizant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface MyAnnotation : NSObject <MKAnnotation>
{
    NSString *title;
    NSString *subtitle;
    UIImage *proImage;
    NSInteger tag;
    NSInteger DictTag;
    
    CLLocationCoordinate2D coordinate;
}
@property ( nonatomic, copy) NSString *title;
@property ( nonatomic, copy)NSString *subtitle;
@property ( nonatomic, copy)UIImage *proImage;
@property ( nonatomic, assign)NSInteger tag;
@property ( nonatomic, assign)NSInteger DictTag;
@property (nonatomic) CLLocationCoordinate2D coordinate;

- (id)initWithTitle:(NSString*)strTitle andSubtitle:(NSString *)strSubtitle andImage:(UIImage *)proImg andCoordinate:(CLLocationCoordinate2D)coord;


@end
