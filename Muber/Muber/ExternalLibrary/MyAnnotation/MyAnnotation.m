//
//  MyAnnotation.m
//  MDLHR
//
//  Created by Bhavesh virani on 21/10/14.
//  Copyright (c) 2014 Dignizant. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation
@synthesize title,subtitle,proImage;
@synthesize coordinate;
@synthesize tag;
@synthesize DictTag;

- (id)initWithTitle:(NSString*)strTitle andSubtitle:(NSString *)strSubtitle andImage:(UIImage *)proImg andCoordinate:(CLLocationCoordinate2D)coord
{
    
    if (self = [super init]) {
        self.title = strTitle;//[strTitle copy];
        self.subtitle=strSubtitle;
        self.proImage=[UIImage imageNamed:@"bg_map_pin_club.png"];;
        self.coordinate = coord;
    }
    return self;
}
-(CLLocationCoordinate2D)coord
{
    return coordinate;
}
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate
{
    coordinate = newCoordinate;
    
}

@end
