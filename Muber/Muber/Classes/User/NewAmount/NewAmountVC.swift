//
//  NewAmountVC.swift
//  Muber
//
//  Created by Jaydeep on 02/04/18.
//

import UIKit
import GoogleMaps

class NewAmountVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var viewOfGoogleMaps: GMSMapView!
    @IBOutlet weak var lblNewInformation: UILabel!
    @IBOutlet weak var lblVehicleStyleTitle: UILabel!
    @IBOutlet weak var lblVehicleType: UILabel!
    @IBOutlet weak var lblBrandTitle: UILabel!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var lblYearTitle: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var imgOfVehicle: UIImageView!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var btnTotlaPrice: UILabel!
    @IBOutlet weak var btnContinueOutlet: CustomButton!
    @IBOutlet weak var lblResumeTitle: UILabel!
    @IBOutlet weak var lblInfoMessage: UILabel!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupGoogleMaps(view: viewOfGoogleMaps)
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "New_Amount_key"))
        
        self.lblResumeTitle.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.lblResumeTitle.text = mapping.string(forKey: "Resumen_key")
        self.lblResumeTitle.textColor = MySingleton.sharedManager.themeRedColor
        
        self.lblInfoMessage.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.lblInfoMessage.text = mapping.string(forKey: "Resume_Info_key")
        self.lblInfoMessage.textColor = .white
       
        self.lblNewInformation.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.lblNewInformation.text = mapping.string(forKey: "New_information_key")
        self.lblNewInformation.textColor = MySingleton.sharedManager.themeLightTextColor
        
        [ lblBrandName,lblYear,lblVehicleName].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
            lbl?.textColor = .black
            
        }
        
        [lblVehicleStyleTitle,lblBrandTitle,lblYearTitle].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
            lbl?.textColor =  MySingleton.sharedManager.themeRedColor
        }
        
        self.lblVehicleStyleTitle.text = mapping.string(forKey: "Vehicle_style_key")
        self.lblBrandTitle.text = mapping.string(forKey: "Brand_key")
        self.lblYearTitle.text = mapping.string(forKey: "Year_key")
       
        self.btnContinueOutlet.setTitle(mapping.string(forKey: "CONTINUE_key"), for: .normal)
        self.btnContinueOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.btnContinueOutlet.setTitleColor(.white, for: .normal)
    }

    //MARK:- Action Zone
    
    @IBAction func btnInfoAction(_ sender: Any) {
    }
    
    @IBAction func btnContinueAction(_ sender: Any)
    {
        let obj = userStoryboard.instantiateViewController(withIdentifier: "ConfirmDriverVC") as! ConfirmDriverVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}
