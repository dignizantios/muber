//
//  SelectTyreVC.swift
//  Muber
//
//  Created by Jaydeep on 04/04/18.
//

import UIKit

class SelectTyreVC: UIViewController {
    
    //MARK:- Variable Declration
    
    //MARK:- Outlet Zone

    @IBOutlet weak var btnTopLeftOutlet: UIButton!
    @IBOutlet weak var btnTopRightOutlet: UIButton!
    @IBOutlet weak var btnBottomLeftOutlet: UIButton!
    @IBOutlet weak var btnBottomRightOutlet: UIButton!
    @IBOutlet weak var btnContinueOutlet: CustomButton!
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Select_tyre_key"))
        
        self.btnContinueOutlet.backgroundColor = MySingleton.sharedManager.themeRedColor
        self.btnContinueOutlet.setTitle(mapping.string(forKey: "CONTINUE_key"), for: .normal)
        self.btnContinueOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
    }

    //MARK:- Action Zone

    @IBAction func btnSelectTyreAction(_ sender: UIButton)
    {
        if sender == btnTopLeftOutlet
        {
            if sender.isSelected == false
            {
                sender.isSelected = true
            }
            else
            {
                sender.isSelected = false
            }
        }
        else if sender == btnTopRightOutlet
        {
            if sender.isSelected == false
            {
                sender.isSelected = true
            }
            else
            {
                sender.isSelected = false
            }
        }
        else if sender == btnBottomLeftOutlet
        {
            if sender.isSelected == false
            {
                sender.isSelected = true
            }
            else
            {
                sender.isSelected = false
            }
        }
        else if sender == btnBottomRightOutlet
        {
            if sender.isSelected == false
            {
                sender.isSelected = true
            }
            else
            {
                sender.isSelected = false
            }
        }
    }
    
    @IBAction func btnContinueAction(_ sender: Any)
    {
        if objBookRide.isComeFromAgainBooking == true
        {
            let obj = userStoryboard.instantiateViewController(withIdentifier: "NewAmountVC") as! NewAmountVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else{
            let obj = userStoryboard.instantiateViewController(withIdentifier: "SourceVC") as! SourceVC
            self.navigationController?.pushViewController(obj, animated: true)
        }      
    }
    
}
