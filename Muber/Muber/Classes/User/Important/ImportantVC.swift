//
//  ImportantVC.swift
//  Muber
//
//  Created by Jaydeep on 03/04/18.
//

import UIKit

class ImportantVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var isComeFromMachinery = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnOKoutlet: CustomButton!
    
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    
    
    //MARK:- Setup UI
    
    func setupUI()
    {
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Important_key"))
        
        self.lblTitle.text = mapping.string(forKey: "Important_detail_key")
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.lblTitle.textColor = MySingleton.sharedManager.themeRedColor
        
        self.lblDescription.text = mapping.string(forKey: "Important_Description_key")
        self.lblDescription.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.lblDescription.textColor = .white
        
        self.btnOKoutlet.setTitle(mapping.string(forKey: "OK_key"), for: .normal)
        self.btnOKoutlet.setTitleColor(.white, for: .normal)
        self.btnOKoutlet.backgroundColor = MySingleton.sharedManager.themeRedColor
    }

    //MARK:- Action Zone
    
    @IBAction func btnOKAction(_ sender: Any)
    {
        let obj = HeightWeightApprox()
        obj.deleApproxHeight = self
        obj.modalPresentationStyle = .overFullScreen
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: true, completion: nil)
    }
   
}

//MARK:- Delegate
extension ImportantVC :delegateApproxHeight
{
    func setValue(load: String, height: String)
    {
        let obj = userStoryboard.instantiateViewController(withIdentifier: "HisCraneVC") as! HisCraneVC
        obj.isType = 1
        obj.isComeFromMachinery = isComeFromMachinery
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
