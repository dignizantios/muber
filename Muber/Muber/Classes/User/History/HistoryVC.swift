//
//  HistoryVC.swift
//  Muber
//
//  Created by Jaydeep on 23/03/18.
//

import UIKit

class HistoryVC: UIViewController {
    
    //MARK:- Varible Declartion
    
      let revealController = SWRevealViewController()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblHistory: UITableView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnHeader: UIButton!
    

    //MARK:- ViewLife cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        btnHeader.addTarget(self.revealViewController, action: #selector(revealController.revealToggle(_:)), for: .touchUpInside)
        
        self.lblHeader.backgroundColor = MySingleton.sharedManager.themeNaviColor
        self.lblHeader.textColor = .white
        self.lblHeader.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
        self.lblHeader.text = mapping.string(forKey: "History_key")
        
     //   setupNavigationBarWithSideMenu(titleText: mapping.string(forKey: "History_key"))
        
        self.tblHistory.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
        self.lblTitle.text = mapping.string(forKey: "Your_History_key")
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.lblTitle.textColor = .black
    }
  
}

//MARK:- Tablewview Delegate

extension HistoryVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        return cell
    }
}
