//
//  SelectTypeVC.swift
//  Muber
//
//  Created by Jaydeep on 26/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class SelectTypeVC: UIViewController {
    
    //MARK:- Variable Declaration
    
     var arrCraneType:[JSON] = []
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblCraneType: UITableView!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var heightOfTblType: NSLayoutConstraint!
    
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupUI()
        setupVehicleTypeArray()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
      
    }
    
    
    
    //MARK:- Setup UI
    
    func setupUI()
    {
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Select_key")!)
        
        self.lblTitle.text = mapping.string(forKey: "Select_crane_type_key")
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 18)
        
        self.lblWarning.text = mapping.string(forKey: "For_your_convinience_we_offer_2_type_of_cranes_key")
        self.lblWarning.font = MySingleton.sharedManager.getFontForTypeBoldWithSize(fontSize: 17)
        
        self.tblCraneType.tableFooterView = UIView()
        
    }
    
    //MARK:- Setup Category Array
    
    func setupVehicleTypeArray()
    {
        arrCraneType = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "PLATAFORMA_key"))
        dict["image"] = "ic_type_of_crane_plateform"
        arrCraneType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "GRUA_DE_CADENA_key"))
        dict["image"] = "ic_type_of_crane_chain_crain"
        arrCraneType.append(dict)
        
        print("arrCategory \(arrCraneType)")
        self.tblCraneType.reloadData()
    }

}

//MARK:- TableView Delegate Datasource

extension SelectTypeVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCraneType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleTypeCell") as! VehicleTypeCell
        let dict = arrCraneType[indexPath.row]
        cell.lblVehicleType.text = dict["name"].stringValue
        cell.imgOfVehicleType.image = UIImage(named: dict["image"].stringValue)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if objBookRide.isComeFromAgainBooking == true
        {
            let obj = userStoryboard.instantiateViewController(withIdentifier: "NewAmountVC") as! NewAmountVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else{
            let obj = userStoryboard.instantiateViewController(withIdentifier: "SourceVC") as! SourceVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height/2
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height/2
    }
}

