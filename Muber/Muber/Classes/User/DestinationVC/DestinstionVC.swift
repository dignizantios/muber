//
//  DestinstionVC.swift
//  Muber
//
//  Created by Jaydeep on 28/03/18.
//

import UIKit
import GoogleMaps

class DestinstionVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var heightOfBottomView: NSLayoutConstraint!
    @IBOutlet weak var viewOfGoogleMaps: GMSMapView!
    @IBOutlet weak var txtDestinationLocation: CustomTextField!
    @IBOutlet weak var lblInfo: UILabel!
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.viewOfGoogleMaps.delegate = self
        
        self.txtDestinationLocation.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.txtDestinationLocation.placeHolderColor = MySingleton.sharedManager.themeGrayHeaderColor
        self.txtDestinationLocation.placeholder = mapping.string(forKey: "Destination_Location_key")
        self.txtDestinationLocation.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.txtDestinationLocation.delegate = self
        
        self.lblInfo.textColor = .black
        self.lblInfo.text = mapping.string(forKey: "You_can_also_drag_the_pin_to_set_the_location_key")
        
        setupGoogleMaps(view: viewOfGoogleMaps)
        
        setUpPinOnMap(lat: userCurrentLocation?.coordinate.latitude ?? 0.0, long:userCurrentLocation?.coordinate.longitude ?? 0.0 )
        appDelegate.setUpQuickLocationUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        let obj = userStoryboard.instantiateViewController(withIdentifier: "ConfirmRideVC") as! ConfirmRideVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}

//MARK:- Google Delegate View

extension DestinstionVC:GMSMapViewDelegate
{
    //MARK:- Setup User Current Location
    
    func setUpPinOnMap(lat:Double,long:Double)
    {
        let cameraCoord = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.viewOfGoogleMaps.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: 18)
        let updateCamera = GMSCameraUpdate.setTarget(cameraCoord, zoom: 18)
        self.viewOfGoogleMaps.animate(with: updateCamera)
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        
        print("latitude")
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            self.heightOfBottomView.constant = 0
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        })
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        print("Defalult latitude \(position.target.latitude)")
        print("Defalult longitude \(position.target.longitude)")
        
        destinationLocation = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        // selectedLocation.latitude  = position.target.latitude
        //  selectedLocation.longitude  = position.target.longitude
        let str = reverseGeoCoding(Latitude: position.target.latitude, Longitude: position.target.longitude)
        self.view.endEditing(true)
        
    }
    func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Latitude, Longitude)
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                
                let lines = address.lines as? [String]
                
                currentAddress = (lines?.joined(separator: ",") ?? "")
                //currentAddress = "\(address.thoroughfare)! , \(address.subLocality)! , \(address.locality)!"
                self.txtDestinationLocation.text = currentAddress
                strDestinationAddress = currentAddress
                print("Address - ",currentAddress)
                UIView.animate(withDuration: 0.5, animations: {
                    self.heightOfBottomView.constant = 80
                    self.view.layoutIfNeeded()
                })
                
            }
        }
        return currentAddress
    }
}

//MARK:- Textfield delegate

extension DestinstionVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK:- get current location

extension DestinstionVC:CurrentLocationDelegate
{
    func didUpdateLocation(lat: Double!, lon: Double!) {
        print("Lat:\(lat) ,lon :\(lon)")
    }
    
}

