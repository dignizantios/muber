//
//  ConfirmRideVC.swift
//  Muber
//
//  Created by Jaydeep on 28/03/18.
//

import UIKit
import GoogleMaps

class ConfirmRideVC: UIViewController {
    
    
    //MARK:- Varable Declaration
    
    let sourceMarker = GMSMarker()
    let destinationMarker = GMSMarker()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var viewOfGoogleMaps: GMSMapView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRequestOutlet: CustomButton!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var imgOfVehicleType: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var heightOfCalculationView: NSLayoutConstraint!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.heightOfCalculationView.constant = 0      
       
        setupGoogleMaps(view: viewOfGoogleMaps)
        
        let obj = ServiceFoundVC()
        obj.ServiceDelegate = self
        obj.SearchStatus = "0"
        obj.modalPresentationStyle = .overFullScreen
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
      
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Setup UI
    
    func setUpUI()
    {
        
        setUpPinOnMap(lat: sourceLocation?.coordinate.latitude ?? 0.0, long: sourceLocation?.coordinate.longitude ?? 0.0, type: 0)
        setUpPinOnMap(lat: destinationLocation?.coordinate.latitude ?? 0.0, long: destinationLocation?.coordinate.longitude ?? 0.0, type: 1)
        
        
        self.viewOfGoogleMaps.delegate = self
        
        self.lblTitle.text = mapping.string(forKey: "Estimate_key")
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
        self.lblTitle.textColor = .black
        
        self.lblVehicleName.text = mapping.string(forKey: "Estimate_key")
        self.lblVehicleName.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 16)
        self.lblVehicleName.textColor = MySingleton.sharedManager.themeLightTextColor
        
        self.lblPrice.text = "$ 10000"
        self.lblPrice.font = MySingleton.sharedManager.getFontForTypeBoldWithSize(fontSize: 22)
        self.lblPrice.textColor = .black
        
        self.btnRequestOutlet.setTitle(mapping.string(forKey: "Request_key"), for: .normal)
        self.btnRequestOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.btnRequestOutlet.setTitleColor(.white, for: .normal)
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnInfoAction(_ sender: Any)
    {
        let obj = userStoryboard.instantiateViewController(withIdentifier: "FairDetailVC") as! FairDetailVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnRequestAction(_ sender: Any)
    {
        let obj = ServiceFoundVC()
        obj.ServiceDelegate = self
     //   obj.isComeFromRouteSearch = false
        obj.SearchStatus = "1"
        obj.modalPresentationStyle = .overFullScreen
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
       
    }
}

//MARK:- Google Delegate View

extension ConfirmRideVC:GMSMapViewDelegate
{
    func setUpPinOnMap(lat:Double,long:Double,type:Int)
    {
        if type == 0
        {
            sourceMarker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
            let sview = UIView()
            sview.frame = CGRect(x: 0, y: 0, width: 200, height: 60)
            let img = UIImageView(image: #imageLiteral(resourceName: "ic_star_location"))
            img.frame = CGRect(x: 0, y: 40, width: 20, height: 20)
            sview.addSubview(img)
            let vw = Bundle.main.loadNibNamed("SourceInfoView", owner: self, options: nil)?[0] as? SourceInfoView
            vw?.lblSourcesName.text = strSourceAddress
            vw?.frame = CGRect(x: 30, y: 0, width: 135, height: 40)
            sview.addSubview(vw!)
            sourceMarker.iconView = sview
            sourceMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            sourceMarker.map = viewOfGoogleMaps
            
         //   sourceMarker.icon = #imageLiteral(resourceName: "ic_star_location")
         /*   let vw = Bundle.main.loadNibNamed("SourceInfoView", owner: self, options: nil)?[0] as? SourceInfoView
            vw?.lblSourcesName.text = strSourceAddress
            let image = UIImageView(image: #imageLiteral(resourceName: "ic_star_location"))
            vw?.addSubview(image)
            sourceMarker.iconView = vw*/
            
         //   self.viewOfGoogleMaps.selectedMarker = sourceMarker
        /*    let vw = Bundle.main.loadNibNamed("SourceInfoView", owner: self, options: nil)?[0] as? SourceInfoView
            vw?.lblSourcesName.text = strSourceAddress
            let point = viewOfGoogleMaps.projection.point(for: sourceMarker.position)
            let pointInNewView = vw?.convert(point, from: viewOfGoogleMaps)
            vw?.frame = CGRect(x: (pointInNewView?.x)!, y: (pointInNewView?.y)!, width: (vw?.frame.size.width)!, height: (vw?.frame.size.height)!)
            self.viewOfGoogleMaps.addSubview(vw!)
            self.viewOfGoogleMaps.bringSubview(toFront: vw!)*/
            
        }
        else
        {
            destinationMarker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
            let sview = UIView()
            sview.frame = CGRect(x: 0, y: 0, width: 200, height: 60)
            let vw = Bundle.main.loadNibNamed("DestinationInfoView", owner: self, options: nil)?[0] as? DestinationInfoView
            vw?.lblDestinationName.text = strSourceAddress
            vw?.frame = CGRect(x: 0, y: 0, width: 135, height: 40)
            sview.addSubview(vw!)
            let img = UIImageView(image:#imageLiteral(resourceName: "ic_end_location"))
            img.frame = CGRect(x: 140, y: 40, width: 20, height: 20)
            sview.addSubview(img)
            destinationMarker.iconView = sview
            destinationMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            destinationMarker.map = viewOfGoogleMaps
           
           /* let vw = Bundle.main.loadNibNamed("DestinationInfoView", owner: self, options: nil)?[0] as? DestinationInfoView
            vw?.lblDestinationName.text = strDestinationAddress
            let image = UIImageView(image: #imageLiteral(resourceName: "ic_end_location"))
            vw?.addSubview(image)
            destinationMarker.iconView = vw*/
            
          //  self.viewOfGoogleMaps.selectedMarker = destinationMarker
         /*   self.viewOfGoogleMaps.camera = GMSCameraPosition.camera(withTarget: destinationMarker.position, zoom: 12)
            let point = viewOfGoogleMaps.projection.point(for: destinationMarker.position)
            let pointInNewView = vw?.convert(point, from: viewOfGoogleMaps)
            vw?.frame = CGRect(x: (pointInNewView?.x)!, y: (pointInNewView?.y)!, width: (vw?.frame.size.width)!, height: (vw?.frame.size.height)!)
            self.viewOfGoogleMaps.addSubview(vw!)
            self.viewOfGoogleMaps.bringSubview(toFront: vw!)*/
        }
         self.viewOfGoogleMaps.camera = GMSCameraPosition.camera(withTarget: sourceMarker.position, zoom: 12)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("posotion \(position)")
        print(" \(position)")
    }
}

extension ConfirmRideVC:CommonServiceDelegate
{
    func setName(strType: String) {
        if strType == "0"
        {
            UIView.animate(withDuration: 0.5, animations: {
                self.heightOfCalculationView.constant = 205
                self.view.layoutIfNeeded()
                self.view.layoutSubviews()
                self.setUpUI()
            })
        }
        else if strType == "1"
        {
            let obj = ServiceFoundVC()
            obj.ServiceDelegate = self
            obj.SearchStatus = "2"
            obj.modalPresentationStyle = .overFullScreen
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: false, completion: nil)
        }
        else{
             let obj = userStoryboard.instantiateViewController(withIdentifier: "ConfirmDriverVC") as! ConfirmDriverVC
              self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
}

