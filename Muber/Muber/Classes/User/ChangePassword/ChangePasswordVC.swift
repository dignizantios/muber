//
//  ChangePasswordVC.swift
//  Muber
//
//  Created by Jaydeep on 17/04/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import LinearProgressBarMaterial

class ChangePasswordVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dictData:JSON?
    
     //MARK:- Outlet Zone
    @IBOutlet weak var lblOldPasswordTitle: UILabel!
    @IBOutlet weak var txtOldPassword: CustomTextField!
    @IBOutlet weak var lblNewPasswordTitle: UILabel!
    @IBOutlet weak var txtNewPassword: CustomTextField!
    @IBOutlet weak var lblConfirmPasswordTitle: UILabel!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    @IBOutlet weak var btnContinueOutlet: CustomButton!
    @IBOutlet weak var viewOfOldPassword: UIView!
    @IBOutlet weak var vwProgress: UIView!
    
    //MARK:- view Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewOfOldPassword.isHidden = true
        
        linearBar = LinearProgressBar(frame: CGRect(x: 0, y: 0, width: self.vwProgress.frame.size.width, height: 5))
        configureLinearProgressBarView(linearBar : linearBar)
        self.vwProgress.addSubview(linearBar)
        
        
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Change_password_key"))
        
        [lblOldPasswordTitle,lblNewPasswordTitle,lblConfirmPasswordTitle].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 20)
            lbl?.textColor = .black
        }
        [txtOldPassword,txtNewPassword,txtConfirmPassword].forEach { (txt) in
            txt?.placeholder = mapping.string(forKey: "Enter_here_key")
            txt?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
            txt?.placeHolderColor = MySingleton.sharedManager.themeGrayFontColor
           // txt?.delegate = self
        }
        
        btnContinueOutlet.setTitle(mapping.string(forKey: "Submit_key"), for: .normal)
        btnContinueOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnContinueOutlet.setTitleColor(.white, for: .normal)
        btnContinueOutlet.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        lblNewPasswordTitle.text = mapping.string(forKey: "New_Password_key")
        lblConfirmPasswordTitle.text = mapping.string(forKey: "Confirm_Password_key")
    }
    
    //MARK:- Action Zone
   
    @IBAction func btnContinueAction(_ sender: Any)
    {
        if (self.txtNewPassword.text?.isEmpty)!
        {
            toastMessage(title: mapping.string(forKey: "Please_enter_new_password_key"))
        }
        else if !(self.txtNewPassword.text == "")
        {
            if (self.txtNewPassword.text?.count)! < 6
            {
                toastMessage(title: mapping.string(forKey: "Password_must_be_at_least_6_characterslong_key"))
            }
            else if self.txtConfirmPassword.text == ""
            {
                toastMessage(title: mapping.string(forKey: "Please_enter_confirm_password_key"))
            }
            else
            {
                if(!(self.txtNewPassword.text == self.txtConfirmPassword.text))
                {
                    toastMessage(title: mapping.string(forKey: "Password_and_confirmation_password_must_match_key"))
                }
                else
                {
                    self.view.endEditing(true)
                    self.btnContinueOutlet.isUserInteractionEnabled = false
                    UserChangePassword()
                }
            }
        }
    }
}

//MARK:- Textfield Delegate

extension ChangePasswordVC: UITextFieldDelegate
{
    
}

//MARK:- Call API

extension ChangePasswordVC
{
    func UserChangePassword()
    {
        var kURL = String()
        kURL = "\(MuberURL)\(userChangePassword)"
       
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param =  ["lang": isSpanish,
                          "new_password": self.txtNewPassword.text ?? "",
                          "user_id": dictData?["user_id"].stringValue ?? "",
                       //   "access_token": dictData?["access_token"].stringValue ?? "",
                          "forgot_password": "1",
                          "timezone": timeZoneIdentifiers]
            
            print("parm \(param)")
            //  show
            linearBar.startAnimation()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                linearBar.stopAnimation()
                self.btnContinueOutlet.isUserInteractionEnabled = true
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == "1"
                    {
                        toastMessage(title: json["msg"].stringValue)
                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                        for aViewController in viewControllers {
                            if aViewController is ViewController {                                self.navigationController!.popToViewController(aViewController, animated: true)
                            }
                        }
                        
                    }
                    else
                    {
                        toastMessage(title: json["msg"].stringValue)
                    }
                }
                else{
                    toastMessage(title: mapping.string(forKey: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            toastMessage(title: mapping.string(forKey: "No_internet_connection,_please_try_again_later_key"))
        }
    }
}
