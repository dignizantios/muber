//
//  FairDetailVC.swift
//  Muber
//
//  Created by Jaydeep on 31/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class FairDetailVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var tblDetails: UITableView!
    
    @IBOutlet weak var lblBasicFareTitle: UILabel!
    @IBOutlet weak var lblBasicTotal: UILabel!
    @IBOutlet weak var lblBookingFareTitle: UILabel!
    @IBOutlet weak var lblBookingTotal: UILabel!
    @IBOutlet weak var lblMinimumFareTitle: UILabel!
    @IBOutlet weak var lblMinimumFareTotal: UILabel!
    @IBOutlet weak var lblPerKilometerTitle: UILabel!
    @IBOutlet weak var lblPerKilometerTotal: UILabel!
    
    //MARK:- Varaible Declaration
    
    var arrayUser: [JSON] = []
    
    
    
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        self.dictForUser()
        
        tblDetails.register(UINib(nibName: "fairDetailsCell", bundle: nil), forCellReuseIdentifier: "fairDetailsCell")
        
        self.tblDetails.tableFooterView = UIView()
        
        
    }
    
    //MARK:- Setup UI
    
    func setUpUI()
    {
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Fair_details_key"))
    }
    
    func dictForUser()
    {
        
        var dict = JSON()
        dict["key"].stringValue = mapping.string(forKey: "Basic_fare_key")
        dict["value"].stringValue = "200"
        self.arrayUser.append(dict)
        
        dict = JSON()
        dict["key"].stringValue = mapping.string(forKey: "Booking_fare_key")
        dict["value"].stringValue = "200"
        self.arrayUser.append(dict)
        
        dict = JSON()
        dict["key"].stringValue = mapping.string(forKey: "Minimum_fare_key")
        dict["value"].stringValue = "200"
        self.arrayUser.append(dict)
        
        dict = JSON()
        dict["key"].stringValue = mapping.string(forKey: "Per_kilometer_key")
        dict["value"].stringValue = "200"
        self.arrayUser.append(dict)
        
    }
}

extension FairDetailVC: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayUser.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "fairDetailsCell") as! fairDetailsCell
        
        let dict = arrayUser[indexPath.row]
        
        cell.lblTitle.text = dict["key"].stringValue
        cell.lblValue.text = dict["value"].stringValue
        
        return cell
    }
    
    
}
