//
//  ConfirmDriverVC.swift
//  Muber
//
//  Created by Jaydeep on 29/03/18.
//

import UIKit
import GoogleMaps

class ConfirmDriverVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var viewOfGoogleMaps: GMSMapView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblRatting: UILabel!
    @IBOutlet weak var imgOfUserProfile: CustomImageView!
    @IBOutlet weak var btnDriveOutlet: UIButton!     
    @IBOutlet weak var btnDriveInformationOutlet: UIButton!
    @IBOutlet weak var btnAgainBookingOutlet: UIButton!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
         setupUI()
    }
    
    
    //MARK:- Setup UI
    
    func setupUI()
    {
         setUpPinOnMap(lat: sourceLocation?.coordinate.latitude ?? 0.0, long: sourceLocation?.coordinate.longitude ?? 0.0, type: 1)
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.viewOfGoogleMaps.delegate = self
        setupGoogleMaps(view: viewOfGoogleMaps)
        
        self.lblDriverName.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.lblDriverName.textColor = .black
        
        self.lblRatting.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
        self.lblRatting.textColor = MySingleton.sharedManager.themeStatusColor
       
        self.btnDriveOutlet.backgroundColor = MySingleton.sharedManager.themeNaviColor
        self.btnDriveOutlet.setTitle(mapping.string(forKey: "Confirm_the_driver_key"), for: .normal)
        self.btnDriveOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.btnDriveOutlet.setTitleColor(.white, for: .normal)
        
        self.btnDriveInformationOutlet.isHidden = true
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnDriveAction(_ sender: Any)
    {
        self.btnDriveOutlet.backgroundColor = MySingleton.sharedManager.themeLightTextColor
        self.btnDriveOutlet.setTitle(mapping.string(forKey: "Driver_is_arriving_now_key"), for: .normal)
        self.viewOfGoogleMaps.clear()
        setUpPinOnMap(lat: sourceLocation?.coordinate.latitude ?? 0.0, long: sourceLocation?.coordinate.longitude ?? 0.0, type: 0)
        setUpPinOnMap(lat: destinationLocation?.coordinate.latitude ?? 0.0, long: destinationLocation?.coordinate.longitude ?? 0.0, type: 1)
        self.btnDriveInformationOutlet.isHidden = false
        self.btnAgainBookingOutlet.isHidden = true
    }
    
    @IBAction func btnSetupAgainAction(_ sender: Any)
    {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SubVehicleTypeVC {
                objBookRide.isComeFromAgainBooking = true
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    @IBAction func btnDriveInformationAction(_ sender: Any)
    {
        let obj = DriverInformation()
        obj.modalPresentationStyle = .overCurrentContext
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ConfirmDriverVC:GMSMapViewDelegate
{
    func setUpPinOnMap(lat:Double,long:Double,type:Int)
    {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if type == 1
        {
            marker.icon = #imageLiteral(resourceName: "ic_towig_service_icon_round_map")
        }
        else
        {
             marker.icon = #imageLiteral(resourceName: "ic_star_location")            
        }
        marker.map = viewOfGoogleMaps
        self.viewOfGoogleMaps.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 15)
    }
    
    
}
