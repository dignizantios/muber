//
//  CategoriesVC.swift
//  Muber
//
//  Created by Jaydeep on 22/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


class CategoriesVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrCategory:[JSON] = []
    let revealController = SWRevealViewController()
    
    //MARK:- Outlet Zone
   
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionOfCategory: UICollectionView!
    @IBOutlet weak var lblWarningText: UILabel!
    @IBOutlet weak var heightOfCollectionView: NSLayoutConstraint!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnHeader: UIButton!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupCategoryArray()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.isNavigationBarHidden = true
        collectionOfCategory.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        collectionOfCategory.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UICollectionView {
            print("contentSize:= \(collectionOfCategory.contentSize.height)")
            self.heightOfCollectionView.constant = collectionOfCategory.contentSize.height
            
        }
    }
    
    //MARK:- Setup UI
    
    func setupUI()
    {
         //setupNavigationBarWithSideMenu(titleText: mapping.string(forKey: "CATEGORIES_key")!)
         btnHeader.addTarget(self.revealViewController, action: #selector(revealController.revealToggle(_:)), for: .touchUpInside)
        
        self.lblHeader.backgroundColor = MySingleton.sharedManager.themeNaviColor
        self.lblHeader.textColor = .white
        self.lblHeader.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
        self.lblHeader.text = mapping.string(forKey: "CATEGORIES_key")
        
        
        self.lblTitle.text = mapping.string(forKey: "24/7_Available_key")
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 23)
        
        self.lblWarningText.text = mapping.string(forKey: "Be_sure_to_select_the_correct_type_of_assistance_key")
         self.lblWarningText.font = MySingleton.sharedManager.getFontForTypeBoldWithSize(fontSize: 17)
        
        collectionOfCategory.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellWithReuseIdentifier:"CategoriesCell")
    }
    
    //MARK:- Setup Category Array
    
    func setupCategoryArray()
    {
        arrCategory = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "TOWLING_key"))
        dict["image"] = "ic_towing_category_home_screen"
        arrCategory.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "TIRE_key"))
        dict["image"] = "ic_tire_category_home_screen"
        arrCategory.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "JUMP_START_key"))
        dict["image"] = "ic_jump_start_category_home_screen"
        arrCategory.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = mapping.string(forKey: "LOCKED_OUT_key")
        dict["image"] = "ic_lock_out_category_home_screen"
        arrCategory.append(dict)
        
        print("arrCategory \(arrCategory)")
        self.collectionOfCategory.reloadData()
    }

}

//MARK:- Collection View Delegate & Datasource

extension CategoriesVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        let dict = arrCategory[indexPath.row]
        cell.lblCategoryName.text = dict["name"].stringValue
        cell.imgOfCategories.image = UIImage(named: dict["image"].stringValue)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width / 2) - 0.5
        let height = (collectionView.frame.size.height / 2) - 0.5
        print("cgsize \(CGSize(width: width, height: height))")
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        objBookRide.isComeFromAgainBooking = false
        if indexPath.row == 0
        {
            let obj = userStoryboard.instantiateViewController(withIdentifier: "VehicleTypeVC") as! VehicleTypeVC
           
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if indexPath.row == 2 ||  indexPath.row == 3
        {
            // jump start
            let obj = userStoryboard.instantiateViewController(withIdentifier: "SubVehicleTypeVC") as! SubVehicleTypeVC
            obj.isVehicleType = 4
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else
        {
            // tyre
            let obj = userStoryboard.instantiateViewController(withIdentifier: "SubVehicleTypeVC") as! SubVehicleTypeVC
            obj.isVehicleType = 0
            obj.isComeFromTyre = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
}
