//
//  HisCraneVC.swift
//  Muber
//
//  Created by Jaydeep on 03/04/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class HisCraneVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var isType = Int()
    var arrCraneType:[JSON] = []
    var isComeFromMachinery = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTypeCrainTitle: UILabel!
    @IBOutlet weak var lblDesciption: UILabel!
    @IBOutlet weak var btnOKOutlet: CustomButton!
    @IBOutlet weak var tblHisCrane: UITableView!
    @IBOutlet weak var heightOfDoneButton: NSLayoutConstraint!
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()       
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
    }
    
    //MARK:- Setup UI
    
    func setupUI()
    {
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "His_Crane_key"))
        
        self.lblTypeCrainTitle.text = mapping.string(forKey: "Type_of_Crane_key")
        self.lblTypeCrainTitle.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.lblTypeCrainTitle.textColor = MySingleton.sharedManager.themeRedColor
        
        self.lblDesciption.text = mapping.string(forKey: "His_crane_title_key")
        self.lblDesciption.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.lblDesciption.textColor = .white
        
        self.btnOKOutlet.setTitle(mapping.string(forKey: "OK_key"), for: .normal)
        self.btnOKOutlet.setTitleColor(.white, for: .normal)
        self.btnOKOutlet.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        if isType == 0
        {
            // simple
            setupForTypeOne()
        }
        else if isType == 1
        {
            // select  2 & 3 option
            if isComeFromMachinery == false
            {
                // come from heavy
                setupForTypeTwo()
            }
            else{
                // come from machinery
                setupForTypeMachiney()
            }
            
            self.lblDesciption.text = mapping.string(forKey: "His_crane_sub_title_key")
            self.btnOKOutlet.isHidden = true
            self.heightOfDoneButton.constant = 0
        }
        else if isType == 2
        {
            //come from above 2
            setupForTypeThree()
        }
        else if isType == 3
        {
             //come from above 3
            setupForTypeFour()
        }
        
        self.tblHisCrane.tableFooterView = UIView()
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnOKAction(_ sender: Any)
    {
        if isType == 0 || isType == 2 || isType == 3
        {
            if objBookRide.isComeFromAgainBooking == true
            {
                let obj = userStoryboard.instantiateViewController(withIdentifier: "NewAmountVC") as! NewAmountVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
            else
            {
                let obj = userStoryboard.instantiateViewController(withIdentifier: "SourceVC") as! SourceVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
    }
    
    //MARK:- Setup Category Array
    
    func setupForTypeOne()
    {
        arrCraneType = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "PLATAFORMA_key"))
        dict["image"] = "ic_crane_for_heavy"
        arrCraneType.append(dict)
        
        print("arrCategory \(arrCraneType)")
        self.tblHisCrane.reloadData()
    }
    
    func setupForTypeTwo()
    {
        arrCraneType = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "PLATAFORMA_key"))
        dict["image"] = "ic_type_of_crane_plateform"
        arrCraneType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Hook_Crane_key"))
        dict["image"] = "ic_type_of_crane_chain_crain"
        arrCraneType.append(dict)
        
        print("arrCategory \(arrCraneType)")
        self.tblHisCrane.reloadData()
    }
    
    func setupForTypeMachiney()
    {
        arrCraneType = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "CRANE_PLATFORM_key"))
        dict["image"] = "ic_type_of_crane_plateform"
        arrCraneType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Lowboy_key"))
        dict["image"] = "ic_crane_lowboy"
        arrCraneType.append(dict)
        
        print("arrCategory \(arrCraneType)")
        self.tblHisCrane.reloadData()
    }
    
    func setupForTypeThree()
    {
        arrCraneType = []
        
        if isComeFromMachinery == false{
            var dict = JSON()
            dict["name"] = JSON(mapping.string(forKey: "PLATAFORMA_key"))
            dict["image"] = "ic_type_of_crane_plateform"
            arrCraneType.append(dict)
        }
        else
        {
            var dict = JSON()
            dict["name"] = JSON(mapping.string(forKey: "CRANE_PLATFORM_key"))
            dict["image"] = "ic_type_of_crane_plateform"
            arrCraneType.append(dict)
        }
        
        print("arrCategory \(arrCraneType)")
        self.tblHisCrane.reloadData()
    }
    
    func setupForTypeFour()
    {
        arrCraneType = []
        
        if isComeFromMachinery == false{
            var dict = JSON()
            dict["name"] = JSON(mapping.string(forKey: "GRUA_DE_CADENA_key"))
            dict["image"] = "ic_type_of_crane_chain_crain"
            arrCraneType.append(dict)
        }
        else{
            var dict = JSON()
            dict["name"] = JSON(mapping.string(forKey: "Lowboy_key"))
            dict["image"] = "ic_crane_lowboy"
            arrCraneType.append(dict)
        }
        
        
        print("arrCategory \(arrCraneType)")
        self.tblHisCrane.reloadData()
    }

}

//MARK:- TableView Delegate Datasource

extension HisCraneVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCraneType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleTypeCell") as! VehicleTypeCell
        let dict = arrCraneType[indexPath.row]
        cell.lblVehicleType.text = dict["name"].stringValue
        cell.imgOfVehicleType.image = UIImage(named: dict["image"].stringValue)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if isType == 1
        {
            if indexPath.row == 0
            {
                let obj = userStoryboard.instantiateViewController(withIdentifier: "HisCraneVC") as! HisCraneVC
                obj.isType = 2
                obj.isComeFromMachinery = isComeFromMachinery
                self.navigationController?.pushViewController(obj, animated: true)
            }
            else{
                let obj = userStoryboard.instantiateViewController(withIdentifier: "HisCraneVC") as! HisCraneVC
                obj.isType = 3
                obj.isComeFromMachinery = isComeFromMachinery
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
      /*  if isType == 1
        {
            return tableView.frame.size.height/2
        }*/
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
       /* if isType == 1
        {
            return tableView.frame.size.height/2
        }*/
        return UITableViewAutomaticDimension
    }
}
