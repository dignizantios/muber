//
//  OTPVC.swift
//  Muber
//
//  Created by Jaydeep on 16/04/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import LinearProgressBarMaterial

class OTPVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var strOTPCode = String()
    var dictData = JSON()
    var isComeFromFP = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewOfOTPTxt: VPMOTPView!
    @IBOutlet weak var btnContinueOutlet: CustomButton!
    @IBOutlet weak var btnResendOTPOutlet: UIButton!
    @IBOutlet weak var vwProgress: UIView!
    

    //MARK:- ViewLife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        linearBar = LinearProgressBar(frame: CGRect(x: 0, y: 0, width: self.vwProgress.frame.size.width, height: 5))
        configureLinearProgressBarView(linearBar : linearBar)
        self.vwProgress.addSubview(linearBar)
        
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "OTP_key"))
        
        self.lblTitle.text = mapping.string(forKey: "Enter_OTP_that_you_received_by_SMS_key")
        self.lblTitle.textColor = MySingleton.sharedManager.themeGrayFontColor
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 18)
        
        btnContinueOutlet.setTitle(mapping.string(forKey: "CONTINUE_key"), for: .normal)
        btnContinueOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnContinueOutlet.setTitleColor(.white, for: .normal)
        btnContinueOutlet.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        let attTitle = attributedString(string1: mapping.string(forKey: "Didn't_receive_OTP_key"), FontColor: MySingleton.sharedManager.themeGrayFontColor, Font: MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 18))
        btnResendOTPOutlet.setAttributedTitle(attTitle, for: .normal)
       
        OTPsetup()
    }
    
    //MARK:- Private Zone
    
    func OTPsetup()
    {
        viewOfOTPTxt.otpFieldFont = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        viewOfOTPTxt.otpFieldsCount = 4
        viewOfOTPTxt.otpFieldDefaultBorderColor = MySingleton.sharedManager.themeRedColor
        viewOfOTPTxt.otpFieldEnteredBorderColor = MySingleton.sharedManager.themeRedColor
        viewOfOTPTxt.otpFieldErrorBorderColor = MySingleton.sharedManager.themeRedColor
        viewOfOTPTxt.otpFieldDisplayType = .square
        viewOfOTPTxt.otpFieldBorderWidth = 2
        viewOfOTPTxt.delegate = self
        viewOfOTPTxt.initalizeUI()        
    }
   
    //MARK:- Action Zone
    
    @IBAction func btnContinueAction(_ sender: Any)
    {
        if strOTPCode.count == 0
        {
            toastMessage(title: mapping.string(forKey: "Please_enter_OTP_key"))
        }
        else if strOTPCode.count < 4
        {
            toastMessage(title: mapping.string(forKey: "Please_enter_valid_OTP_key"))
        }
        else
        {
            self.btnContinueOutlet.isUserInteractionEnabled = false
            UserVerifyOTP()
        }
    }
    
    @IBAction func btnResendOTPAction(_ sender: Any)
    {
        self.btnResendOTPOutlet.isUserInteractionEnabled = false
        UserResendOTP()
    }
    
}

//MARK:- OTP Textfields Delegate

extension OTPVC: VPMOTPViewDelegate {
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return true
        // return enteredOTP == "12345"
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        print("OTPString: \(otpString)")
        self.strOTPCode = otpString
    }
}

//MARK:- Call Service

extension OTPVC
{
    func UserVerifyOTP()
    {
        var kURL = String()
        
        if isComeFromFP == true
        {
            kURL = "\(MuberURL)\(userVerifyOTP)"
        }
        else{
            kURL = "\(MuberURL)\(userRegiVerifyOTP)"
        }
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param =  ["lang": isSpanish,
                          "otp": strOTPCode,
                          "user_id": dictData["user_id"].stringValue]
            
            print("parm \(param)")
            //  show
            linearBar.startAnimation()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                linearBar.stopAnimation()
                self.btnContinueOutlet.isUserInteractionEnabled = true
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == "1"
                    {
                        if self.isComeFromFP == true
                        {
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                            vc.dictData = self.dictData
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else
                        {
                            toastMessage(title: json["msg"].stringValue)
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                            for aViewController in viewControllers {
                                if aViewController is ViewController {
                                    self.navigationController!.popToViewController(aViewController, animated: true)
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        toastMessage(title: json["msg"].stringValue)
                    }
                }
                else{
                    toastMessage(title: mapping.string(forKey: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            toastMessage(title: mapping.string(forKey: "No_internet_connection,_please_try_again_later_key"))
        }
    }
    
    func UserResendOTP()
    {
        let kURL = "\(MuberURL)\(userResendOTP)"
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param =  ["lang": isSpanish,
                          "user_id": dictData["user_id"].stringValue]
            
            print("parm \(param)")
            //  show
            linearBar.startAnimation()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                linearBar.stopAnimation()
                self.btnResendOTPOutlet.isUserInteractionEnabled = true
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == "1"
                    {
                        
                    }
                    else
                    {
                        toastMessage(title: json["msg"].stringValue)
                    }
                }
                else{
                    toastMessage(title: mapping.string(forKey: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            toastMessage(title: mapping.string(forKey: "No_internet_connection,_please_try_again_later_key"))
        }
    }
}
