//
//  SubVehicleTypeVC.swift
//  Muber
//
//  Created by Jaydeep on 23/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class SubVehicleTypeVC: UIViewController {
    
    //MARK:- Varible Delcaration
    
    var arrSubVehicleType:[JSON] = []
    var isVehicleType = Int()
    var isComeFromTyre = Bool()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var collectionViewVehicleType: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupUI()
        
        if isVehicleType == 0
        {
            setupVehicleMediumTypeArray() // MEDIUM
        }
        else if isVehicleType == 1
        {
            setupVehicleHeavyTypeArray()  //HEAVY
        }
        else{
            setupVehicleMachineryTypeArray() //MACHINERY
        }
    }

    //MARK:- Setup UI
    
    func setupUI()
    {
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Vehicle_Type_key"))
        
        self.lblTitle.text = mapping.string(forKey: "Select_vehicle_type_key")
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 18)
        
        collectionViewVehicleType.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellWithReuseIdentifier:"CategoriesCell")
    }
    
    //MARK:- Setup Category Array
    
    func setupVehicleMediumTypeArray()
    {
        arrSubVehicleType = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Cedan_key"))
        dict["image"] = "ic_select_medium_type_vehicle_cedan"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Hetchback_key"))
        dict["image"] = "ic_select_medium_type_vehicle_hetchback"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Pick_up_cabina_sencilla_key"))
        dict["image"] = "ic_select_medium_type_vehicle_pickup_sebina"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = mapping.string(forKey: "Pick_up_double_cabina_key")
        dict["image"] = "ic_select_medium_type_vehicle_pickup_dobila"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Minivan_key"))
        dict["image"] = "ic_select_medium_type_vehicle_minivan"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = mapping.string(forKey: "Todo_terreno_key")
        dict["image"] = "ic_select_medium_type_vehicle_todo_terreno"
        arrSubVehicleType.append(dict)
        
        print("arrSubVehicleType \(arrSubVehicleType)")
        self.collectionViewVehicleType.reloadData()
    }
    
    func setupVehicleHeavyTypeArray()
    {
        arrSubVehicleType = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Cabezal_key"))
        dict["image"] = "ic_select_crane_heavy_vehicle_cabezal"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Camion_2_Ejes_key"))
        dict["image"] = "ic_select_crane_heavy_vehicle_camion_2"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Camion_1_Ejes_key"))
        dict["image"] = "ic_select_crane_heavy_vehicle_camion_1"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = mapping.string(forKey: "Extra_van_key")
        dict["image"] = "ic_select_crane_heavy_vehicle_extra_van"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Bus_key"))
        dict["image"] = "ic_select_crane_heavy_vehicle_bus"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = mapping.string(forKey: "Mini_Bus_key")
        dict["image"] = "ic_select_crane_heavy_vehicle_mini_bus"
        arrSubVehicleType.append(dict)
        
        print("arrSubVehicleType \(arrSubVehicleType)")
        self.collectionViewVehicleType.reloadData()
    }
    
    func setupVehicleMachineryTypeArray()
    {
        arrSubVehicleType = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Nibeladora_key"))
        dict["image"] = "ic_machinary_vehicle_nibeladora"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Exabadora_key"))
        dict["image"] = "ic_machinary_vehicle_exabadora"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Bulldozer_key"))
        dict["image"] = "ic_machinary_vehicle_bulldozer"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = mapping.string(forKey: "Compactador_key")
        dict["image"] = "ic_machinary_vehicle_compactador"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Backhoe_key"))
        dict["image"] = "ic_machinary_vehicle_backhoe"
        arrSubVehicleType.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = mapping.string(forKey: "Montacargas_key")
        dict["image"] = "ic_machinary_vehicle_montacargas"
        arrSubVehicleType.append(dict)
        
        print("arrSubVehicleType \(arrSubVehicleType)")
        self.collectionViewVehicleType.reloadData()
    }


}

extension SubVehicleTypeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSubVehicleType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        let dict = arrSubVehicleType[indexPath.row]
        cell.lblCategoryName.text = dict["name"].stringValue
        cell.imgOfCategories.image = UIImage(named: dict["image"].stringValue)
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = (collectionView.frame.size.width / 2) - 0.5
        let height = (collectionView.frame.size.height / 3) - 0.5
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isVehicleType == 0
        {
            let obj = userStoryboard.instantiateViewController(withIdentifier: "VehicleInfoVc") as! VehicleInfoVc
            if isComeFromTyre == true
            {
                 obj.isComeFromTyre = 0
            }
            else{
                obj.isComeFromTyre = 10
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if isVehicleType == 1
        {
            if indexPath.row == 2 || indexPath.row == 3
            {
                let obj = userStoryboard.instantiateViewController(withIdentifier: "ImportantVC") as! ImportantVC
                obj.isComeFromMachinery = false
                self.navigationController?.pushViewController(obj, animated: true)
            }
            else{
                let obj = userStoryboard.instantiateViewController(withIdentifier: "HisCraneVC") as! HisCraneVC
                obj.isType = 0
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
        else if isVehicleType == 4
        {
            let obj = userStoryboard.instantiateViewController(withIdentifier: "VehicleInfoVc") as! VehicleInfoVc
            obj.isComeFromTyre = 4
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else
        {
            let obj = userStoryboard.instantiateViewController(withIdentifier: "ImportantVC") as! ImportantVC
            obj.isComeFromMachinery = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
   
}


