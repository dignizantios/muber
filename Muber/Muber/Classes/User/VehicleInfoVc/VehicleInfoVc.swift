//
//  VehicleInfoVc.swift
//  Muber
//
//  Created by Jaydeep on 24/03/18.
//

import UIKit

class VehicleInfoVc: UIViewController {
    
    //MARK:- Variable Declaration
    
    var isComeFromTyre = Int()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSelectBrandOutlet: CustomButton!
    @IBOutlet weak var btnYearoutlet: CustomButton!
    @IBOutlet weak var btnNextOutlet: CustomButton!
    
    //MARK:- View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         setUpUI()
    }
    
    //MARK:- SetUPp UI
    
    func setUpUI()
    {
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Vehicle_Information_key"))
        
        self.lblTitle.text = mapping.string(forKey: "New_Vehicle_Information_key")
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.lblTitle.textColor = .black
        
         [self.btnSelectBrandOutlet,self.btnYearoutlet].forEach({
            $0?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
            $0?.setTitleColor(MySingleton.sharedManager.themeLightTextColor, for: .normal)
            $0?.backgroundColor = UIColor.init(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
            $0?.borderColor = UIColor.init(red: 220/255, green: 220/255, blue: 200/255, alpha: 1.0)
         //   $0?.imageEdgeInsets = UIEdgeInsetsMake(0, (230*UIScreen.main.bounds.size.width)/320, 0, 0)
         })
       
        self.btnYearoutlet.setTitle(mapping.string(forKey: "Year_key"), for: .normal)
        
        self.btnSelectBrandOutlet.setTitle(mapping.string(forKey: "Select_Brand_key"), for: .normal)
        
        self.btnNextOutlet.setTitle(mapping.string(forKey: "NEXT_key"), for: .normal)
        self.btnNextOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.btnNextOutlet.setTitleColor(.white, for: .normal)
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnSelectBrandAction(_ sender: Any) {
    }
    
   
    @IBAction func btnSelectYearAction(_ sender: Any) {
    }
    
    @IBAction func btnNextAction(_ sender: Any)
    {
        if isComeFromTyre == 0
        {
            // tyre
            let obj = userStoryboard.instantiateViewController(withIdentifier: "SelectTyreVC") as! SelectTyreVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if isComeFromTyre == 4
        {
            // tyre
            if objBookRide.isComeFromAgainBooking == true
            {
                let obj = userStoryboard.instantiateViewController(withIdentifier: "NewAmountVC") as! NewAmountVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
            else{
                let obj = userStoryboard.instantiateViewController(withIdentifier: "SourceVC") as! SourceVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
            
        }
        else{
            let obj = userStoryboard.instantiateViewController(withIdentifier: "SelectTypeVC") as! SelectTypeVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
       
    }
}
