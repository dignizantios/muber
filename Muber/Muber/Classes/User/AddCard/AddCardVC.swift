//
//  AddCardVC.swift
//  Muber
//
//  Created by Jaydeep on 29/03/18.
//

import UIKit

class AddCardVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblCardHolderNameTitle: UILabel!
    @IBOutlet weak var txtCardHolderName: CustomTextField!
    @IBOutlet weak var lblCardNumberTitle: UILabel!
    @IBOutlet weak var txtCardNumber: CustomTextField!
    @IBOutlet weak var lblMonthTitle: UILabel!
    @IBOutlet weak var txtMonthName: CustomTextField!
    @IBOutlet weak var lblYearTitle: UILabel!
    @IBOutlet weak var txtYear: CustomTextField!
    @IBOutlet weak var lblCVVTitle: UILabel!
    @IBOutlet weak var txtCVV: CustomTextField!
    @IBOutlet weak var btnAddOutlet: CustomButton!
    
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        
    }
    
    func setUpUI()
    {
       setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Add_card_key"))
        [lblCardHolderNameTitle,lblCardNumberTitle,lblCVVTitle,lblYearTitle,lblMonthTitle].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
            lbl?.textColor = .black
        }
        
        [txtCardHolderName,txtCardNumber,txtYear,txtMonthName,txtCVV].forEach { (txtfield) in
            txtfield?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
            txtfield?.textColor = .black
            txtfield?.placeHolderColor = MySingleton.sharedManager.themeLightTextColor
            txtfield?.delegate = self
        }
        
        self.txtCardHolderName.placeholder = mapping.string(forKey: "Enter_here_key")
        self.txtCardNumber.placeholder = mapping.string(forKey: "Enter_here_key")
        self.txtMonthName.placeholder = mapping.string(forKey: "Select_key")
        self.txtYear.placeholder = mapping.string(forKey: "Select_key")
        self.txtCVV.placeholder = mapping.string(forKey: "Enter_here_key")
        
        self.lblCardHolderNameTitle.text = mapping.string(forKey: "Card_holder_name_key")
        self.lblCardNumberTitle.text = mapping.string(forKey: "Card_number_key")
        self.lblMonthTitle.text = mapping.string(forKey: "Month_key")
        self.lblYearTitle.text = mapping.string(forKey: "Year_key")
        self.lblCVVTitle.text = mapping.string(forKey: "CVV_key")
        
        self.btnAddOutlet.setTitle(mapping.string(forKey: "ADD_key"), for: .normal)
        self.btnAddOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.btnAddOutlet.setTitleColor(.white, for: .normal)
    }

    //MARK:- Action Zone
    
    @IBAction func btnAddAction(_ sender: Any) {
    }
    
}

extension AddCardVC:UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
