//
//  SourceVC.swift
//  Muber
//
//  Created by Jaydeep on 27/03/18.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SourceVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var heightOfBottomView: NSLayoutConstraint!
    @IBOutlet weak var viewOfGoogleMaps: GMSMapView!
    @IBOutlet weak var txtSourceLocation: CustomTextField!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var tblAutoComplete: UITableView!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.viewOfGoogleMaps.delegate = self
        
        self.txtSourceLocation.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.txtSourceLocation.placeHolderColor = MySingleton.sharedManager.themeGrayHeaderColor
        self.txtSourceLocation.placeholder = mapping.string(forKey: "Source_Location_key")
        self.txtSourceLocation.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.txtSourceLocation.delegate = self
        
        self.lblInfo.textColor = .black
        self.lblInfo.text = mapping.string(forKey: "You_can_also_drag_the_pin_to_set_the_location_key")
        
        setupGoogleMaps(view: viewOfGoogleMaps)
        
        setUpPinOnMap(lat: userCurrentLocation?.coordinate.latitude ?? 0.0, long:userCurrentLocation?.coordinate.longitude ?? 0.0 )
        appDelegate.setUpQuickLocationUpdate()
    }
    
    //MARK:- Action Zone

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnDoneAction(_ sender: Any) {
        let obj = userStoryboard.instantiateViewController(withIdentifier: "DestinstionVC") as! DestinstionVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK:- Google Delegate View

extension SourceVC:GMSMapViewDelegate{
   
    
    func setUpPinOnMap(lat:Double,long:Double)
    {
        let cameraCoord = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.viewOfGoogleMaps.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: 18)
        let updateCamera = GMSCameraUpdate.setTarget(cameraCoord, zoom: 18)
        self.viewOfGoogleMaps.animate(with: updateCamera)
    }

    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        
        print("latitude")
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            self.heightOfBottomView.constant = 0
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        })
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        print("Defalult latitude \(position.target.latitude)")
        print("Defalult longitude \(position.target.longitude)")
        
        sourceLocation = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
       // selectedLocation.latitude  = position.target.latitude
      //  selectedLocation.longitude  = position.target.longitude
        let str = reverseGeoCoding(Latitude: position.target.latitude, Longitude: position.target.longitude)
        self.view.endEditing(true)
        
    }
    func reverseGeoCoding(Latitude : Double,Longitude : Double) -> String
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Latitude, Longitude)
        var currentAddress = String()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                let lines = address.lines as? [String]
                currentAddress = (lines?.joined(separator: ",") ?? "")
                //currentAddress = "\(address.thoroughfare)! , \(address.subLocality)! , \(address.locality)!"
                self.txtSourceLocation.text = currentAddress
                strSourceAddress = currentAddress
                print("Address - ",currentAddress)
                UIView.animate(withDuration: 0.5, animations: {
                    self.heightOfBottomView.constant = 80
                    self.view.layoutIfNeeded()
                })
            }
        }
        return currentAddress
    }
}

//MARK:- Textfield delegate

extension SourceVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        acController.modalPresentationStyle = .popover
        acController.modalTransitionStyle = .crossDissolve
        acController.view.backgroundColor = .clear
        present(acController, animated: false, completion: nil)
        
        return true
        
    }
    
}

//MARK:- get current location

extension SourceVC:CurrentLocationDelegate
{
    func didUpdateLocation(lat: Double!, lon: Double!) {
        print("Lat:\(lat) ,lon :\(lon)")
    }
    
}

extension SourceVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        print("Place Lat : \(place.coordinate.latitude)")
        print("Place Lon : \(place.coordinate.longitude)")
        
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        
        txtSourceLocation.text = place.formattedAddress!
        setUpPinOnMap(lat:place.coordinate.latitude,long:place.coordinate.longitude)
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: \(error)")
        dismiss(animated: true, completion: nil)
    }
    
    // User cancelled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("Autocomplete was cancelled.")
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    
  
    
}


