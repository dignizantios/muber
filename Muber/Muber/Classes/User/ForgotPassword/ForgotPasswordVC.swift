//
//  ForgotPasswordVC.swift
//  Muber
//
//  Created by Jaydeep on 16/04/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import LinearProgressBarMaterial

class ForgotPasswordVC: UIViewController {
    
    //MARK:- Varialbe Declaration
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lbltxtFieldTitle: UILabel!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var btnContinueOutlet: CustomButton!
    @IBOutlet weak var vwProgress: UIView!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
       
    }
   
    //MARK:- Private Zone
    
    func setUpUI()
    {
        linearBar = LinearProgressBar(frame: CGRect(x: 0, y: 0, width: self.vwProgress.frame.size.width, height: 5))
        configureLinearProgressBarView(linearBar : linearBar)
        self.vwProgress.addSubview(linearBar)
        
        linearBar = LinearProgressBar(frame: CGRect(x: 0, y: 0, width: self.vwProgress.frame.size.width, height: 5))
        configureLinearProgressBarView(linearBar : linearBar)
        self.vwProgress.addSubview(linearBar)
        
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Forgot_Password_key"))
        
        [lbltxtFieldTitle].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 20)
            lbl?.textColor = .black
            lbl?.text = mapping.string(forKey: "Forgot_Password_key")
        }
        
        [txtMobileNumber].forEach { (txt) in
            txt?.placeholder = mapping.string(forKey: "Enter_here_key")
            txt?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
            txt?.placeHolderColor = MySingleton.sharedManager.themeGrayFontColor
            txt?.delegate = self
        }
        
        btnContinueOutlet.setTitle(mapping.string(forKey: "CONTINUE_key"), for: .normal)
        btnContinueOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnContinueOutlet.setTitleColor(.white, for: .normal)
        btnContinueOutlet.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        addDoneButtonOnKeyboard(textfield: txtMobileNumber)
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnContinueAction(_ sender: Any)
    {
        if ((self.txtMobileNumber.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            toastMessage(title: mapping.string(forKey: "Please_enter_mobile_number_key"))
        }
        else if (self.txtMobileNumber.text?.count)! < 10
        {
            toastMessage(title: mapping.string(forKey: "Please_enter_valid_mobile_number_key"))
        }
        else{
            self.btnContinueOutlet.isUserInteractionEnabled = false
            sendForgotPasswordOTP()
        }
    }
}

extension ForgotPasswordVC: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobileNumber
        {
            let limitLength = 10
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= limitLength // Bool
        }
        return true
    }
}

//MARK:- Service

extension ForgotPasswordVC
{
    func sendForgotPasswordOTP()
    {
        let kURL = "\(MuberURL)\(userForgotPassword)"
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param =  ["lang": isSpanish,
                          "timezone": timeZoneIdentifiers,
                          "mobile": self.txtMobileNumber.text ?? ""]
            
            print("parm \(param)")
            linearBar.startAnimation()
            
            CommonService().Service(url: kURL, param: param, completion: { (respones) in
                linearBar.stopAnimation()
                self.btnContinueOutlet.isUserInteractionEnabled = true
                if let json = respones.value
                {
                    print("json \(json)")
                    
                    if json["flag"].stringValue == "1"
                    {
                        //print("json \(json)")
                        
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                        vc.dictData = json["data"]
                        vc.isComeFromFP = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else
                    {
                        toastMessage(title: json["msg"].stringValue)
                    }
                }
                else{
                    toastMessage(title: mapping.string(forKey: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            toastMessage(title: mapping.string(forKey: "No_internet_connection,_please_try_again_later_key"))
        }
    }
}
