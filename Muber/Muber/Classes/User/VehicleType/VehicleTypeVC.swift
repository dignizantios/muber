//
//  VehicleTypeVC.swift
//  Muber
//
//  Created by Jaydeep on 23/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class VehicleTypeVC: UIViewController {
    
    //MARK:- Varible Delcaration
    
    var arrVehicleType:[JSON] = []
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblVehicleType: UITableView!
    @IBOutlet weak var heightOfTblType: NSLayoutConstraint!
    @IBOutlet weak var lblWarning: UILabel!
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupVehicleTypeArray()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tblVehicleType.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblVehicleType.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblVehicleType.contentSize.height)")
           // self.heightOfTblType.constant = tblVehicleType.contentSize.height
            
        }
    }
    
    //MARK:- Setup UI
    
    func setupUI()
    {
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Vehicle_Type_key")!)
        
        self.lblTitle.text = mapping.string(forKey: "Select_vehicle_type_from_below_key")
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 18)
        
        self.lblWarning.text = mapping.string(forKey: "Sure_to_select_the_type_of_car_in_its_category_key")
        self.lblWarning.font = MySingleton.sharedManager.getFontForTypeBoldWithSize(fontSize: 17)
        
        self.tblVehicleType.tableFooterView = UIView()
       
    }
    
    //MARK:- Setup Category Array
    
    func setupVehicleTypeArray()
    {
        arrVehicleType = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "MEDIUM_key"))
        dict["image"] = "ic_medium_vehicle_type"
        arrVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "HEAVY_key"))
        dict["image"] = "ic_heavy_vehicle_type"
        arrVehicleType.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "MACHINERY_key"))
        dict["image"] = "ic_machinary_vehicle_type"
        arrVehicleType.append(dict)
        
        print("arrCategory \(arrVehicleType)")
        self.tblVehicleType.reloadData()
    }

}

extension VehicleTypeVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVehicleType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleTypeCell") as! VehicleTypeCell
        let dict = arrVehicleType[indexPath.row]
        cell.lblVehicleType.text = dict["name"].stringValue
        cell.imgOfVehicleType.image = UIImage(named: dict["image"].stringValue)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height/3
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height/3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = userStoryboard.instantiateViewController(withIdentifier: "SubVehicleTypeVC") as! SubVehicleTypeVC
        objBookRide.isComeFromAgainBooking = false
        obj.isVehicleType = indexPath.row
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
