//
//  RatingVC.swift
//  Muber
//
//  Created by Jaydeep on 31/03/18.
//

import UIKit
import FloatRatingView

class RatingVC: UIViewController {
    
    //MARK:- Varible Declaration
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgOfUserProfile: CustomImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblRatingTitle: UILabel!
    @IBOutlet weak var viewOfReview: FloatRatingView!
    @IBOutlet weak var txtWriteComment: CustomTextview!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var btnSubmitOutlet: CustomButton!
    
    //MARK:- View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    //MARK:- Setup UI
    
    func setupUI()
    {
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "Rating_key"))
        
        self.lblUserName.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.lblUserName.textColor = .black
        
        self.lblRating.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
        self.lblRating.textColor = MySingleton.sharedManager.themeStatusColor
        
        self.lblRatingTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.lblRatingTitle.textColor = MySingleton.sharedManager.themeLightTextColor
        self.lblRatingTitle.text = mapping.string(forKey: "Give_your_rating_key")
        
        self.lblPlaceholder.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
        self.lblPlaceholder.textColor = MySingleton.sharedManager.themeLightTextColor
        self.lblPlaceholder.text = mapping.string(forKey: "Write_comment_key")
        
        self.txtWriteComment.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
        self.txtWriteComment.textColor = MySingleton.sharedManager.themeLightTextColor
        self.txtWriteComment.delegate = self
        
        self.btnSubmitOutlet.setTitle(mapping.string(forKey: "Submit_key"), for: .normal)
        self.btnSubmitOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.btnSubmitOutlet.setTitleColor(.white, for: .normal)
    }
    

    //MARK:- Action Zone
    
    @IBAction func btnSubmitAction(_ sender: Any)
    {
    }
    

}

//MARK:- TextView Delegate

extension RatingVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if self.txtWriteComment.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else{
            self.lblPlaceholder.isHidden = true
        }
    }
}
