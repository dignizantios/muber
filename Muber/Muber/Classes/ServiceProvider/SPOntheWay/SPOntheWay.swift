//
//  SPOntheWay.swift
//  Muber
//
//  Created by YASH on 03/04/18.
//

import UIKit
import GoogleMaps


class SPOntheWay: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var viewOfGoogleMap: GMSMapView!
    
    @IBOutlet weak var btnArrived: CustomButton!
    
     @IBOutlet weak var btnSideUserInfo: UIButton!
    
    //MARK: - variable
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.viewOfGoogleMap.delegate = self
        setupGoogleMaps(view: viewOfGoogleMap)
        
        setUpPinOnMap(lat: userCurrentLocation?.coordinate.latitude ?? 0.0, long: userCurrentLocation?.coordinate.longitude ?? 0.0, type: 0)
        
        setUpPinOnMap(lat: destinationLocation?.coordinate.latitude ?? 0.0, long: destinationLocation?.coordinate.longitude ?? 0.0, type: 1)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBarWithBackButton(titleText: "Time")
    }
    

}

extension SPOntheWay
{
    
    //MARK: - setup UI
    
    func setupUI()
    {
        self.btnArrived.setTitle(mapping.string(forKey: "ARRIVED_key"), for: .normal)
        self.btnArrived.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.btnArrived.setTitleColor(.white, for: .normal)
    }
    
    //MARK: - IBAction
    
    @IBAction func btnArrivedTapped(_ sender: UIButton)
    {
        let vc = SPStoryboard.instantiateViewController(withIdentifier: "SPCompletedRideVc") as! SPCompletedRideVc
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSideUserInfoTapped(_ sender: UIButton)
    {
        let obj = DriverInformation()
        obj.isComeFromSP = "0"
        obj.modalPresentationStyle = .overCurrentContext
        self.present(obj, animated: false, completion: nil)
    }
    
}


//MARK:- Google Delegate View

extension SPOntheWay:GMSMapViewDelegate
{
    func setUpPinOnMap(lat:Double,long:Double,type:Int)
    {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if type == 0
        {
            // marker.icon = #imageLiteral(resourceName: "ic_star_location")
            let vw = Bundle.main.loadNibNamed("DestinationInfoView", owner: self, options: nil)?[0] as? DestinationInfoView
            vw?.lblDestinationName.text = mapping.string(forKey: "Your_location_key")
            marker.icon = #imageLiteral(resourceName: "ic_towig_service_icon_round_map")
            marker.iconView = vw
            marker.userData = "1"
        }
        else
        {
            
            marker.icon = #imageLiteral(resourceName: "ic_end_location")
            marker.userData = "2"
        }
        
        marker.map = viewOfGoogleMap
        marker.tracksViewChanges  = true
        marker.tracksInfoWindowChanges = true
        
        // self.viewOfGoogleMaps.selectedMarker = marker
        self.viewOfGoogleMap.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 15)
        
    }
    
}









