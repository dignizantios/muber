//
//  SPSideMenuVc.swift
//  Muber
//
//  Created by YASH on 23/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage

class SPSideMenuVc: UIViewController {

    //MARK: -  outlet
    
    @IBOutlet weak var vwHeader: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgName: UILabel!
    @IBOutlet weak var tblSPSideMenu: UITableView!
    
    //MARK: - Variable
    
    var arrSideMenu : [JSON] = []
    var isComeFromLogin = Bool()
    
    //MARK : - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isComeFromLogin == true
        {
           self.setupUserDetails()
        }
        else
        {
            self.setupDetails()
        }
        
        tblSPSideMenu.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        
        tblSPSideMenu.tableFooterView = UIView()
        tblSPSideMenu.tableHeaderView = vwHeader

        revealViewController().tapGestureRecognizer()
        revealViewController().panGestureRecognizer()
        
        self.setupUI()
    }

    
    override func viewWillAppear(_ animated: Bool)
    {
        
        self.navigationController?.navigationBar.isHidden = true
        let rewel = self.revealViewController()
        rewel?.frontViewController.view.isUserInteractionEnabled = false
    rewel?.frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        let rewel = self.revealViewController()
        rewel?.frontViewController.view.isUserInteractionEnabled = true
    }
    
}




extension SPSideMenuVc
{
    func setupUI()
    {
        self.imgName.font = MySingleton.sharedManager.getFontForTypeLatoBlackWithSize(fontSize: 20)
    }
    
    func setupDetails()
    {
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Home_key"))
        dict["image"] = "ic_home_sidemenu"
        dict["selected"] = "1"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "History_key"))
        dict["image"] = "ic_history_sidemenu"
        dict["selected"] = "0"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Paymnet_key"))
        dict["image"] = "ic_payment_sidemenu"
        dict["selected"] = "0"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Filter_destination_key"))
        dict["image"] = "ic_filter_destination_sidemenu"
        dict["selected"] = "0"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Setting_key"))
        dict["image"] = "ic_setting_sidemenu"
        dict["selected"] = "0"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Logout_key"))
        dict["image"] = "ic_logout_sidemenu"
        dict["selected"] = "0"
        arrSideMenu.append(dict)
        
    }
    
    func setupUserDetails()
    {
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Home_key"))
        dict["image"] = "ic_home_sidemenu"
        dict["selected"] = "1"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "History_key"))
        dict["image"] = "ic_history_sidemenu"
        dict["selected"] = "0"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Setting_key"))
        dict["image"] = "ic_setting_sidemenu"
        dict["selected"] = "0"
        arrSideMenu.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Logout_key"))
        dict["image"] = "ic_logout_sidemenu"
        dict["selected"] = "0"
        arrSideMenu.append(dict)
        
    }
    
    //MARK: - Navigation Controller
    
    func navigateToController(obj : UIViewController)
    {
        let rearNavigation = UINavigationController(rootViewController: obj)
        rearNavigation.isNavigationBarHidden = false
        self.revealViewController().setFront(rearNavigation, animated: true)
        self.revealViewController().revealToggle(animated: true)
    }
    
    
}


extension SPSideMenuVc: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenu.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSPSideMenu.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        
        let dict = arrSideMenu[indexPath.row]
        
        cell.lblSideMenu.text = dict["name"].stringValue
        cell.imgSideMenu.image = UIImage(named: dict["image"].stringValue)
        
        if dict["selected"].stringValue == "0"
        {
            cell.contentView.backgroundColor = .clear
            cell.vwWhite.isHidden = true
        }
        else
        {
            cell.contentView.backgroundColor = MySingleton.sharedManager.themeRedColor
            cell.vwWhite.isHidden = false
        }
        
        if indexPath.row == arrSideMenu.count - 1
        {
            cell.lblArrow.isHidden = true
        }
        else
        {
            cell.lblArrow.isHidden = false
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dict = arrSideMenu[indexPath.row]
        if dict["selected"].stringValue == "0"
        {
            dict["selected"].stringValue = "1"
        }
        else
        {
            dict["selected"].stringValue = "0"
        }
        
        arrSideMenu[indexPath.row] = dict
        
        for i in 0..<arrSideMenu.count
        {
            if(i==indexPath.row)
            {
                arrSideMenu[i]["selected"].stringValue = "1"
                
            }else
            {
                arrSideMenu[i]["selected"].stringValue = "0"
            }
        }
        tblSPSideMenu.reloadData()
        if indexPath.row == 0
        {
            if isComeFromLogin == true
            {
                let categoryVC = userStoryboard.instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
                navigateToController(obj: categoryVC)
            }
            else
            {
                let Sphomevc = SPStoryboard.instantiateViewController(withIdentifier: "SPHomeVc") as! SPHomeVc
                navigateToController(obj: Sphomevc)
            }
            
        }
        else if indexPath.row == 1
        {
            let histroyvc = userStoryboard.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
            navigateToController(obj: histroyvc)
        }
    }
}

