//
//  SPCrainInfoVc.swift
//  Muber
//
//  Created by YASH on 27/03/18.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import TOCropViewController

class SPCrainInfoVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblTypeofCrain: UILabel!
    @IBOutlet weak var txtTypeofCrain: CustomTextField!
    
    @IBOutlet weak var lblCpacityInTone: UILabel!
    @IBOutlet weak var txtCpacityInTone: CustomTextField!
    
    @IBOutlet weak var lblNumberPlate: UILabel!
    @IBOutlet weak var txtNumberPlate: CustomTextField!
    
    @IBOutlet weak var btnADD: CustomButton!
    
    @IBOutlet weak var collectionPhotos: UICollectionView!
    
    //MARK: - Variable
    
    var aryPhotos: [UIImage] = []
    
    var pickerImage = UIImagePickerController()
    
    var positionCount:Int = 0
    
    
    //MARK: - view life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionPhotos.register(UINib(nibName: "CraneInfoCollectionCell", bundle: nil), forCellWithReuseIdentifier:"CraneInfoCollectionCell")

        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "CRAIN_INFORMATION_key"))

    }
    
}


extension SPCrainInfoVc
{
    // MARK: - setup
    
    func setupUI()
    {
        
        lblTypeofCrain.text = mapping.string(forKey: "TYPE_OF_CRAIN_key")
        lblCpacityInTone.text = mapping.string(forKey: "CAPACITY_IN_TONE_key")
        lblNumberPlate.text = mapping.string(forKey: "NUMBER_PLATE_key")
        
        [txtTypeofCrain,txtCpacityInTone,txtNumberPlate].forEach { (txt) in
            txt?.placeholder = mapping.string(forKey: "Select_key")
            txt?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
            txt?.placeHolderColor = MySingleton.sharedManager.themeGrayFontColor
            txt?.delegate = self
            
        }
        
        
        btnADD.setTitle(mapping.string(forKey: "ADD_key"), for: .normal)
        btnADD.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnADD.setTitleColor(.white, for: .normal)
        btnADD.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        pickerImage.delegate = self
        
    }
    
    //MARK: - IBAction
    
    @IBAction func btnAddPhotoTapped(_ sender: UIButton)
    {
        
    }
    
    @IBAction func btnLeftActionTapped(_ sender: UIButton)
    {
        if aryPhotos.count > 0
        {
            print("PostionCount : \(positionCount)")
            
            if positionCount > 0
            {
                positionCount -= 1
                print("After minus PostionCount : \(positionCount)")

                collectionPhotos.scrollToItem(at: IndexPath(item: positionCount , section: 0), at: .left, animated: true)
            }
            
        }
    }
    
    @IBAction func btnRightActionTapped(_ sender: UIButton)
    {
        if aryPhotos.count > 0
        {
            print("PostionCount : \(positionCount)")
            
            if positionCount < aryPhotos.count
            {
                positionCount += 1
                
                print("After Plus PostionCount : \(positionCount)")
                
                 collectionPhotos.scrollToItem(at: IndexPath(item: positionCount, section: 0), at: .right, animated: true)
            }
           
        }
    }
    
    @IBAction func btnAddTapped(_ sender: UIButton)
    {
        let vc = SPStoryboard.instantiateViewController(withIdentifier: "SPCompletedVc") as! SPCompletedVc
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnUpperTapped(sender: UIButton)
    {
        
        print("Sender Tag : \(sender.tag)")
        ShowChooseImageOptions(picker: pickerImage)
        
    }
    
    
}

extension SPCrainInfoVc: UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
}


extension SPCrainInfoVc: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return aryPhotos.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionPhotos.dequeueReusableCell(withReuseIdentifier: "CraneInfoCollectionCell", for: indexPath) as! CraneInfoCollectionCell
        
        cell.btnUpper.tag = indexPath.row
        cell.btnUpper.addTarget(self, action: #selector(btnUpperTapped), for: .touchUpInside)
        
        if indexPath.row == aryPhotos.count
        {
            cell.imgInfo.image = #imageLiteral(resourceName: "ic_upload_your_files")
        }
        else
        {
            cell.imgInfo.image = aryPhotos[indexPath.row]
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let width = collectionView.frame.size.width
        let height = collectionView.frame.size.height
        
        return CGSize(width: width,height:height)
    }
    
}


extension SPCrainInfoVc : UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let selectedImage = info[UIImagePickerControllerOriginalImage]
        let cropVC = TOCropViewController(image: selectedImage as! UIImage)
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: 160)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.red, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.red, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        pickerImage.present(cropVC, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int)
    {
        
        let data = UIImagePNGRepresentation(image)! as NSData
        let selectedImage:UIImage = UIImage(data: data as Data)!
        
        self.aryPhotos.append(selectedImage)
        
        collectionPhotos.reloadData()
        
        if aryPhotos.count > 0
        {
            positionCount = aryPhotos.count
            collectionPhotos.scrollToItem(at: IndexPath(item: aryPhotos.count, section: 0), at: .right, animated: true)
        }
        
        
//        if isEditbtn == 0
//        {
//            if(isImageEdit)
//            {
//                let TempimgDic = TempUplodedimgArray[selectedImageIndex] as! NSMutableDictionary
//                let imageid = TempimgDic["id"] as! String
//                let filename = TempimgDic["filename"] as! String
//                print("imageid-->\(imageid),filename-->\(filename)")
//                for (idex,_) in MyUplodedimgArray.enumerated()
//                {
//                    let MyimgDic = MyUplodedimgArray[idex] as! NSMutableDictionary
//                    let id = MyimgDic["id"] as! String
//                    let name = MyimgDic["filename"] as! String
//                    print("id-->\(id),name-->\(name)")
//                    if(id == imageid && name == filename)
//                    {
//                        if(imageid == "0")
//                        {
//                            /*MyimgDic["is_New"] = "1"
//                             MyimgDic["is_Edit"] = "0"
//                             MyimgDic["is_Deleted"] = "0"*/
//                            MyimgDic["addedImg"] = selectedImage
//                        }else{
//                            MyimgDic["is_New"] = "0"
//                            MyimgDic["is_Edit"] = "1"
//                            MyimgDic["is_Deleted"] = "0"
//                            MyimgDic["addedImg"] = selectedImage
//                        }
//                        MyUplodedimgArray.replaceObject(at: idex, with: MyimgDic)
//                        TempUplodedimgArray.replaceObject(at: selectedImageIndex, with: MyimgDic)
//                        break;
//                    }
//                }
//                print("TempUplodedimgArray : \(TempUplodedimgArray)")
//                print("MyUplodedimgArray : \(MyUplodedimgArray)")
//                self.collectionImgAdd.reloadData()
//            }
//            else
//            {
//                let MyimgDic = NSMutableDictionary()
//                MyimgDic["id"] = "0"
//                MyimgDic["filename"] = "files\(MyUplodedimgArray.count+1)"
//                MyimgDic["name"] = "files\(MyUplodedimgArray.count+1)"
//                MyimgDic["is_New"] = "1"
//                MyimgDic["is_Edit"] = "0"
//                MyimgDic["is_Deleted"] = "0"
//                MyimgDic["addedImg"] = selectedImage
//                MyUplodedimgArray.add(MyimgDic)
//                TempUplodedimgArray.add(MyimgDic)
//                print("TempUplodedimgArray : \(TempUplodedimgArray)")
//                print("MyUplodedimgArray : \(MyUplodedimgArray)")
//                self.collectionImgAdd.reloadData()
//                if(TempUplodedimgArray.count > 0)
//                {
//                    collectionImgAdd.scrollToItem(at: IndexPath(item: TempUplodedimgArray.count, section: 0), at: .right, animated: true)
//                }
//
//            }
//
//        }
//        else
//        {
//            if(isImageEdit)
//            {
//                arrayImg[selectedImageIndex] = selectedImage
//            }
//            else
//            {
//                arrayImg.append(selectedImage)
//
//            }
//            print("ArrImg : \(arrayImg)")
//            self.collectionImgAdd.reloadData()
//            if(arrayImg.count > 0)
//            {
//                collectionImgAdd.scrollToItem(at: IndexPath(item: arrayImg.count, section: 0), at: .right, animated: true)
//            }
//
//        }
        
        cropViewController.dismiss(animated: false, completion: nil)
        pickerImage.dismiss(animated: true, completion: nil)
        
    }
    
}

