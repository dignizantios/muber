//
//  SPPaymentServiceVc.swift
//  Muber
//
//  Created by YASH on 26/03/18.
//

import UIKit

class SPPaymentServiceVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblBank: UILabel!
    @IBOutlet weak var txtBank: CustomTextField!
    
    @IBOutlet weak var lblAccountNumber: UILabel!
    @IBOutlet weak var txtAccountNumber: CustomTextField!
    
    @IBOutlet weak var lblCustomerAccount: UILabel!
    @IBOutlet weak var txtCustomerAccount: CustomTextField!
    
    @IBOutlet weak var lblAccountGardienName: UILabel!
    @IBOutlet weak var txtGardianName: CustomTextField!
    
    @IBOutlet weak var lblDetails: UILabel!
    
    @IBOutlet weak var btnADD: CustomButton!
    
    //MARK: - Variable
    
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "PAYMENT_FOR_SERVICES_key"))
    }

   

}

extension SPPaymentServiceVc
{
    
    //MARK: - Setup
    
    
    func setupUI()
    {
        
        lblBank.text = mapping.string(forKey: "BANK_key")
        lblAccountNumber.text = mapping.string(forKey: "ACCOUNT_NUMBER_key")
        lblCustomerAccount.text = mapping.string(forKey: "CUSTOMER_ACCOUNT_key")
        lblAccountGardienName.text = mapping.string(forKey: "ACCOUNT_GUARDIAN_NAME_key")
        
        self.lblDetails.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.lblDetails.textColor = .white
        
        [txtBank,txtGardianName,txtAccountNumber,txtCustomerAccount].forEach { (txt) in
            txt?.delegate = self
            txt?.placeholder = mapping.string(forKey: "Enter_here_key")
            txt?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
            txt?.placeHolderColor = MySingleton.sharedManager.themeGrayFontColor
        }
        
        addDoneButtonOnKeyboard(textfield: txtAccountNumber)
        addDoneButtonOnKeyboard(textfield: txtCustomerAccount)
        
        self.txtBank.placeholder = mapping.string(forKey: "Select_key")
        
        btnADD.setTitle(mapping.string(forKey: "ADD_key"), for: .normal)
        btnADD.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnADD.setTitleColor(.white, for: .normal)
        btnADD.backgroundColor = MySingleton.sharedManager.themeRedColor
        
    }
    
    
    //MARK: - IBAction
    
    @IBAction func btnAddTapped(_ sender: UIButton)
    {
        
        let vc = SPStoryboard.instantiateViewController(withIdentifier: "SPIDVc") as! SPIDVc
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension SPPaymentServiceVc: UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtBank
        {
            return false
        }
        
        return true
    }
    
    
}



