//
//  RegisterVC.swift
//  Muber
//
//  Created by Jaydeep on 22/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class RegisterVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var btnNaviLogin: UIButton!
    @IBOutlet weak var vwBottomLogin: UIView!
    @IBOutlet weak var btnNaviCreateAccount: UIButton!
    @IBOutlet weak var vwBottomCreate: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var txtMobile: CustomTextField!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    @IBOutlet weak var btnBottomRegister: CustomButton!
    
    //MARK: - Variable
    
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}


extension RegisterVC: SWRevealViewControllerDelegate
{
    func setupUI()
    {
        //upper
        
        btnNaviLogin.setTitle(mapping.string(forKey: "Login_key"), for: .normal)
        btnNaviCreateAccount.setTitle(mapping.string(forKey: "Creat_an_account_key"), for: .normal)
        
        btnNaviCreateAccount.setTitleColor(.black, for: .normal)
        btnNaviCreateAccount.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 25)
        self.vwBottomCreate.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        
        btnNaviLogin.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 25)
        btnNaviLogin.setTitleColor(MySingleton.sharedManager.themeGrayFontColor, for: .normal)
        
        self.vwBottomLogin.backgroundColor = MySingleton.sharedManager.themeGrayFontColor
        
        //bottom
        
        [lblName,lblMobile,lblPassword,lblConfirmPassword].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 20)
            lbl?.textColor = .black
        }
        
        lblName.text = mapping.string(forKey: "NAME_key")
        lblMobile.text = mapping.string(forKey: "MOBILE_key")
        lblPassword.text = mapping.string(forKey: "PASSWORD_key")
        lblConfirmPassword.text = mapping.string(forKey: "CONFIRM_PASSWORD_key")
        
        [txtName,txtMobile,txtPassword,txtConfirmPassword].forEach { (txt) in
            txt?.placeholder = mapping.string(forKey: "Enter_here_key")
            txt?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
            txt?.placeHolderColor = MySingleton.sharedManager.themeGrayFontColor
            txt?.delegate = self
        }
        
        btnBottomRegister.setTitle(mapping.string(forKey: "REGISTER_key"), for: .normal)
        btnBottomRegister.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnBottomRegister.setTitleColor(.white, for: .normal)
        btnBottomRegister.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        addDoneButtonOnKeyboard(textfield: txtMobile)
        
        
    }
    
    @IBAction func btnNaviLoginTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnNaviCreateTapped(_ sender: UIButton)
    {
        
    }
    
    @IBAction func btnBottomRegisterTapped(_ sender: UIButton)
    {
        if ((self.txtName.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            toastMessage(title: mapping.string(forKey: "Please_enter_name_key"))
        }
        else
        {
            if ((self.txtMobile.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
            {
                toastMessage(title: mapping.string(forKey: "Please_enter_mobile_number_key"))
            }
            else if (self.txtMobile.text?.count)! < 10
            {
                toastMessage(title: mapping.string(forKey: "Please_enter_valid_mobile_number_key"))
            }
            else if (self.txtPassword.text?.isEmpty)!
            {
                toastMessage(title: mapping.string(forKey: "Please_enter_password_key"))
            }
            else if !(self.txtPassword.text == "")
            {
                if (self.txtPassword.text?.count)! < 6
                {
                    toastMessage(title: mapping.string(forKey: "Password_must_be_at_least_6_characterslong_key"))
                }
                else if self.txtConfirmPassword.text == ""
                {
                    toastMessage(title: mapping.string(forKey: "Please_enter_confirm_password_key"))
                }
                else
                {
                    if(!(self.txtPassword.text == self.txtConfirmPassword.text))
                    {
                        toastMessage(title: mapping.string(forKey: "Password_and_confirmation_password_must_match_key"))
                    }
                    else
                    {
                        self.view.endEditing(true)
                        let obj = SPStoryboard.instantiateViewController(withIdentifier: "SPtermsAndCondiVc") as! SPtermsAndCondiVc
                        let objUser = UserData()
                        objUser.strName = self.txtName.text ?? ""
                        objUser.strMobile = self.txtMobile.text ?? ""
                        objUser.strPassword = self.txtPassword.text ?? ""
                        obj.objUser = objUser
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                }
            }
        }
       /* let frontViewController = SPStoryboard.instantiateViewController(withIdentifier: "SPHomeVc") as! SPHomeVc
        let rearViewController = SPStoryboard.instantiateViewController(withIdentifier: "SPSideMenuVc") as! SPSideMenuVc
        
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        let rearNavigationController = UINavigationController(rootViewController: rearViewController)
        
        let revealController = SWRevealViewController(rearViewController: rearNavigationController, frontViewController: frontNavigationController)
        
        revealController?.navigationController?.isNavigationBarHidden = true
        revealController?.rearViewRevealWidth = (frontViewController.view.frame.size.width) - 75
        
        revealController?.delegate = self
        
        let window = UIApplication.shared.delegate?.window as? UIWindow
        
        window?.rootViewController = revealController
        window?.makeKeyAndVisible()*/
    }
}

//MARK:- Textfiled Delegate Method

extension RegisterVC: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobile
        {
            let limitLength = 10
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= limitLength // Bool
        }
        return true
    }
}
