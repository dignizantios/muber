//
//  SPLicenseVc.swift
//  Muber
//
//  Created by YASH on 26/03/18.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import TOCropViewController

class SPLicenseVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblTypeofLicense: UILabel!
    @IBOutlet weak var txtTypeofLicense: CustomTextField!
    
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var txtNumber: CustomTextField!
    
    @IBOutlet weak var lblExpiryMonth: UILabel!
    @IBOutlet weak var txtExpiryMonth: CustomTextField!
    
    @IBOutlet weak var lblExpiryYear: UILabel!
    @IBOutlet weak var txtExpiryYear: CustomTextField!
    
    @IBOutlet weak var lblAddPhoto: UILabel!
    
    @IBOutlet weak var lblPleaseuploadReadablePhoto: UILabel!
    
    @IBOutlet weak var btnADD: CustomButton!
    
    @IBOutlet weak var imgPhotos: UIImageView!
    
    //MARK: - Variable
    
    var arrayYear: [JSON] = []
    
    var arrayMonth: [JSON] = []
    
    var isMonthSelect = String()
    var currentYear = Int()
    var currentMonth = Int()
    
    var picker = UIImagePickerController()
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupUI()
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "LICENSE_key"))
    }
    

}


extension SPLicenseVc
{
    
    //MARK: - setup
    
    func setupUI()
    {
        
        lblTypeofLicense.text = mapping.string(forKey: "TYPE_OF_LICENSE_key")
        lblNumber.text = mapping.string(forKey: "NUMBER_key")
        lblExpiryMonth.text = mapping.string(forKey: "EXPIRY_MONTH_key")
        lblExpiryYear.text = mapping.string(forKey: "EXPIRY_YEAR_key")
        
        [txtTypeofLicense,txtExpiryYear,txtExpiryMonth,txtNumber].forEach { (txt) in
            txt?.placeholder = mapping.string(forKey: "Select_key")
            txt?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
            txt?.placeHolderColor = MySingleton.sharedManager.themeGrayFontColor
            txt?.delegate = self
            
        }
        
        txtNumber.placeholder = mapping.string(forKey: "Enter_here_key")
        
        self.lblAddPhoto.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 18)
        self.lblAddPhoto.textColor = MySingleton.sharedManager.themeGrayHeaderColor
        self.lblAddPhoto.text = mapping.string(forKey: "ADD_PHOTO_key")
        
        addDoneButtonOnKeyboard(textfield: txtNumber)
        
        self.lblPleaseuploadReadablePhoto.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 18)
        self.lblPleaseuploadReadablePhoto.textColor = MySingleton.sharedManager.themeGrayFontColor
        self.lblPleaseuploadReadablePhoto.text = mapping.string(forKey: "Please_upload_readable_photo_key")
        
        btnADD.setTitle(mapping.string(forKey: "ADD_key"), for: .normal)
        btnADD.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnADD.setTitleColor(.white, for: .normal)
        btnADD.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        picker.delegate = self
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy"
        
        let strYear = dateformatter.string(from: Date())
        currentYear = Int(strYear)!
        print("year : \(currentYear)")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM"
        
        let strMonth = formatter.string(from: Date())
        currentMonth = Int(strMonth)!
        print("currentMonth : \(currentMonth)")
        
        self.MonthandYear()
        
    }
    
    //MARK: - month year
    
    func MonthandYear()
    {
        for i in 1..<13
        {
            var dict = JSON()
            dict["value"].stringValue = "\(i)"
            arrayMonth.append(dict)
        }
        
        for i in 0..<51
        {
            var dict = JSON()
            dict["value"].stringValue = String(currentYear + i)
            arrayYear.append(dict)
            
        }
        
    }
    
    
    //MARK: - IBAction
    
    @IBAction func btnAddTapped(_ sender: UIButton)
    {
        
        let vc = SPStoryboard.instantiateViewController(withIdentifier: "SPCrainInfoVc") as! SPCrainInfoVc
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func btnUploadPhotoTapped(_ sender: UIButton)
    {
        ShowChooseImageOptions(picker: picker)
    }
    
}


extension SPLicenseVc: UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtExpiryYear
        {
            if (txtExpiryMonth.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
            {
                toastMessage(title: mapping.string(forKey: "Please_select_expiry_month_key"))
                return false
            }
            else
            {
                
                isMonthSelect = "1"
                let pickerController: CommonPickerVC = CommonPickerVC()
                pickerController.pickerDelegate = self
                pickerController.arrayPicker = arrayYear
                pickerController.isComeFromExpiryMonth = false
                pickerController.selectedYear = txtExpiryYear.text ?? "0"
                pickerController.selectedMonth = txtExpiryMonth.text ?? "0"
                pickerController.modalPresentationStyle = .overCurrentContext
                pickerController.modalTransitionStyle = .crossDissolve
                self.present(pickerController, animated: true, completion: nil)
                return false
            }
        }
        else if textField == txtExpiryMonth
        {
            isMonthSelect = "0"
            let pickerController : CommonPickerVC = CommonPickerVC()
            pickerController.pickerDelegate = self
            pickerController.arrayPicker = arrayMonth
            pickerController.isComeFromExpiryMonth = true
            pickerController.selectedYear = txtExpiryYear.text ?? "0"
            pickerController.selectedMonth = txtExpiryMonth.text ?? "0"
            pickerController.modalPresentationStyle = .overCurrentContext
            pickerController.modalTransitionStyle = .crossDissolve
            self.present(pickerController, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
}


extension SPLicenseVc : UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate
{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        let selectedImage = info[UIImagePickerControllerOriginalImage]
        let cropVC = TOCropViewController(image: selectedImage as! UIImage)
        
        cropVC.aspectRatioLockEnabled = true
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: 160)
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.red, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.red, for: .normal)
        cropVC.toolbar.rotateClockwiseButtonHidden = true
        
        picker.present(cropVC, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        imgPhotos.image = image
        
        cropViewController.dismiss(animated: false, completion: nil)
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension SPLicenseVc: CommonPickerDelegate
{
    func setValuePicker(value: String) {
        
        if isMonthSelect == "0"
        {
            txtExpiryMonth.text = value
        }
        else
        {
            txtExpiryYear.text = value
        }
    }
    
    
}
