//
//  SPtermsAndCondiVc.swift
//  Muber
//
//  Created by YASH on 24/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import LinearProgressBarMaterial

class SPtermsAndCondiVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblInformation: UILabel!
    @IBOutlet weak var btnCheckUncheck: UIButton!
    @IBOutlet weak var btnContinue: CustomButton!
    @IBOutlet weak var vwProgress: UIView!

    //MARK: - Variable
   
     var objUser = UserData()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        linearBar = LinearProgressBar(frame: CGRect(x: 0, y: 0, width: self.vwProgress.frame.size.width, height: 5))
        configureLinearProgressBarView(linearBar : linearBar)
        self.vwProgress.addSubview(linearBar)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "TERMS_AND_CONDITIONS_Key"))
        self.setupUI()
        
    }
    
    
    @IBAction func btnCheckUncheckedTapped(_ sender: UIButton)
    {
        if btnCheckUncheck.isSelected == true
        {
            btnCheckUncheck.isSelected = false
        }
        else
        {
            btnCheckUncheck.isSelected = true
        }
    }
    
}

extension SPtermsAndCondiVc
{
    func setupUI()
    {
        self.lblInformation.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 18)
        self.lblInformation.textColor = MySingleton.sharedManager.themeGrayFontColor
        
        btnContinue.setTitle(mapping.string(forKey: "CONTINUE_key"), for: .normal)
        btnContinue.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnContinue.setTitleColor(.white, for: .normal)
        btnContinue.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        [btnCheckUncheck,btnContinue].forEach { (btn) in
            btn?.isUserInteractionEnabled = false
        }
        
        getTermCondition()
        
        
    }
    
    
    @IBAction func btnContinueTapped(_ sender: UIButton)
    {
        if self.btnCheckUncheck.isSelected == false
        {
            toastMessage(title: mapping.string(forKey: "You_must_agree_with_our_T&C_key"))
        }
        else
        {
            UserRegistration()
           
        }
       
    }
    
    
    @IBAction func btnTermsNPolicesTapped(_ sender: UIButton)
    {
        
    }
    
}

//MARK:- Service

extension SPtermsAndCondiVc{
    func UserRegistration()
    {
        let kRegiURL = "\(MuberURL)\(userRegister)"
        print("URL:- \(kRegiURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let strDeviceToken = Defaults.value(forKey: "device_token") as? String ?? ""
            
            let param =  ["lang": isSpanish,
                          "name":objUser.strName,
                          "mobile" :objUser.strMobile,
                          "password" :objUser.strPassword,
                          "is_accept_aggrement" :"1",
                          "device_token": strDeviceToken,
                          "timezone":timeZoneIdentifiers,
                          "register_id":"",
                          "device_type":strDeviceType]
            
            print("parm \(param)")
            //  show
            linearBar.startAnimation()
            
            CommonService().Service(url: kRegiURL, param: param, completion: { (respones) in
                linearBar.stopAnimation()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == "1"
                    {
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                        vc.dictData = json["data"]
                        vc.isComeFromFP = false
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else
                    {
                        toastMessage(title: json["msg"].stringValue)
                    }
                }
                else{
                    toastMessage(title: mapping.string(forKey: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            toastMessage(title: mapping.string(forKey: "No_internet_connection,_please_try_again_later_key"))
        }
    }
    
    func getTermCondition()
    {
        let kRegiURL = "\(MuberURL)\(userTermCondition)"
        print("URL:- \(kRegiURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param =  ["lang": isSpanish]
            
            print("parm \(param)")
            linearBar.startAnimation()
            
            CommonService().Service(url: kRegiURL, param: param, completion: { (respones) in
                linearBar.stopAnimation()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    [self.btnCheckUncheck,self.btnContinue].forEach { (btn) in
                        btn?.isUserInteractionEnabled = true
                    }
                    if json["flag"].stringValue == "1"
                    {
                        //print("json \(json)")
                        self.lblInformation.text = json["data"].stringValue
                    }
                    else
                    {
                        toastMessage(title: json["msg"].stringValue)
                    }
                }
                else{
                    toastMessage(title: mapping.string(forKey: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            toastMessage(title: mapping.string(forKey: "No_internet_connection,_please_try_again_later_key"))
        }
    }
    
}
