
//
//  SPInformationVc.swift
//  Muber
//
//  Created by YASH on 26/03/18.
//

import UIKit

class SPInformationVc: UIViewController {

    //MARK: - Outlet
    
    
    @IBOutlet weak var tblInformation: UITableView!
    
    @IBOutlet weak var btnOk: CustomButton!
    
    
    //MARK: - Variable
    
    
    //MARK: - view life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblInformation.register(UINib(nibName:"SPImportantCell",bundle:nil), forCellReuseIdentifier: "SPImportantCell")

        tblInformation.register(UINib(nibName:"SPImportantHeaderCell",bundle:nil), forCellReuseIdentifier: "SPImportantHeaderCell")

        self.setupUI()
        
        self.tblInformation.contentInset = UIEdgeInsetsMake(0, 0, 70, 0)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "IMPORTANT_key"))
        
    }
    
    

}


extension SPInformationVc
{
    //MARK: - Setup
    
    func setupUI()
    {
        btnOk.setTitle(mapping.string(forKey: "OK_key"), for: .normal)
        btnOk.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnOk.setTitleColor(.white, for: .normal)
        btnOk.backgroundColor = MySingleton.sharedManager.themeRedColor
    }
    
    //MARK: - IBAction
    
    @IBAction func btnOKTapped(_ sender: UIButton)
    {
        
        let vc = SPStoryboard.instantiateViewController(withIdentifier: "SPPaymentServiceVc") as! SPPaymentServiceVc
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}


extension SPInformationVc: UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SPImportantCell") as! SPImportantCell
        
        cell.lblDetails.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SPImportantHeaderCell") as! SPImportantHeaderCell
        
        cell.lblHeader.text = "TOWING"
        
        return cell.contentView
    }
    
    
    
    
    
    
}

