//
//  SPCategoryVC.swift
//  Muber
//
//  Created by YASH on 23/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage

class SPCategoryVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblInformation: UILabel!
    
    @IBOutlet weak var lblOption: UILabel!
    
    @IBOutlet weak var tblListOption: UITableView!
    
    @IBOutlet weak var btnContinue:
    CustomButton!
    
    @IBOutlet weak var tblHeightConstant: NSLayoutConstraint!
    
    
    //MARK: - Variable
    
    var arrayDetails : [JSON] = []

    
    //MARK: - view life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        
        tblListOption.register(UINib(nibName: "SPCategoryCell", bundle: nil), forCellReuseIdentifier: "SPCategoryCell")
        
        self.tblListOption.tableFooterView = UIView()
        
        self.setupDetails()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "CATEGORIES_key"))
        
        tblListOption.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblListOption.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblListOption.contentSize.height)")
            self.tblHeightConstant.constant = tblListOption.contentSize.height
            
        }
    }
    
    @IBAction func btnContinueTapped(_ sender: UIButton) {
        
        let register = SPStoryboard.instantiateViewController(withIdentifier: "SPtermsAndCondiVc") as! SPtermsAndCondiVc
        
        self.navigationController?.pushViewController(register, animated: true)
        
    }
    
}


extension SPCategoryVC
{
    
    func setupUI()
    {
        
        self.lblInformation.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.lblInformation.textColor = .white
        
        self.lblOption.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 18)
        self.lblOption.textColor = MySingleton.sharedManager.themeGrayHeaderColor
        self.lblOption.text = mapping.string(forKey: "OPTIONS_key")
        
        btnContinue.setTitle(mapping.string(forKey: "CONTINUE_key"), for: .normal)
        btnContinue.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnContinue.setTitleColor(.white, for: .normal)
        btnContinue.backgroundColor = MySingleton.sharedManager.themeRedColor
        
    }
    
    func setupDetails()
    {
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Towing_key"))
        dict["selected"] = "0"
        arrayDetails.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Tyre_key"))
        dict["selected"] = "0"
        arrayDetails.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Jump_start_key"))
        dict["selected"] = "0"
        arrayDetails.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Locked_Out_key"))
        dict["selected"] = "0"
        arrayDetails.append(dict)
        
    }
   
    
}


extension SPCategoryVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SPCategoryCell") as! SPCategoryCell
        
        let dict = self.arrayDetails[indexPath.row]
        
        cell.lblOptionTitle.text = dict["name"].stringValue
        cell.btnCheckUncheck.tag = indexPath.row
        
        if dict["selected"].stringValue == "0"
        {
            cell.btnCheckUncheck.isSelected = false
        }
        else
        {
            cell.btnCheckUncheck.isSelected = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dict = self.arrayDetails[indexPath.row]
        
        if dict["selected"].stringValue == "0"
        {
            dict["selected"].stringValue = "1"
        }
        else
        {
            dict["selected"].stringValue = "0"
        }
        
        arrayDetails[indexPath.row] = dict
        
        for i in 0..<arrayDetails.count
        {
            if(i==indexPath.row)
            {
                arrayDetails[i]["selected"].stringValue = "1"
                
            }else
            {
                arrayDetails[i]["selected"].stringValue = "0"
            }
        }
        
        tblListOption.reloadData()
        
    }
    
}

