//
//  SPCompletedRideVc.swift
//  Muber
//
//  Created by YASH on 02/04/18.
//

import UIKit
import GoogleMaps

class SPCompletedRideVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var viewOfGoogleMaps: GMSMapView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnConfirm: CustomButton!
    @IBOutlet weak var lblCrainName: UILabel!
    @IBOutlet weak var imgOfCrainType: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    
    //MARK: - Variable
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUpUI()
        self.viewOfGoogleMaps.delegate = self
        setupGoogleMaps(view: viewOfGoogleMaps)
        setUpPinOnMap(lat: userCurrentLocation?.coordinate.latitude ?? 0.0, long: userCurrentLocation?.coordinate.longitude ?? 0.0, type: 0)
        setUpPinOnMap(lat: destinationLocation?.coordinate.latitude ?? 0.0, long: destinationLocation?.coordinate.longitude ?? 0.0, type: 1)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
}

extension SPCompletedRideVc
{
    
    func setUpUI()
    {
        self.lblTitle.text = mapping.string(forKey: "SERVICE_COMPLETED_key")
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 19)
        self.lblTitle.textColor = MySingleton.sharedManager.themeGreenFontColor
        
        self.lblCrainName.text = mapping.string(forKey: "TOWLING_key")
        self.lblCrainName.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 16)
        self.lblCrainName.textColor = MySingleton.sharedManager.themeLightTextColor
        
        self.lblPrice.text = "$ 10000"
        self.lblPrice.font = MySingleton.sharedManager.getFontForTypeBoldWithSize(fontSize: 22)
        self.lblPrice.textColor = .black
        
        self.btnConfirm.setTitle(mapping.string(forKey: "CONFIRM_key"), for: .normal)
        self.btnConfirm.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.btnConfirm.setTitleColor(.white, for: .normal)
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBAction Method
    
    @IBAction func btnInfoTapped(_ sender: UIButton)
    {
        
        let vc = userStoryboard.instantiateViewController(withIdentifier: "FairDetailVC") as! FairDetailVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func btnConfirmActionTapped(_ sender: UIButton)
    {
        let vc = userStoryboard.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}



//MARK:- Google Delegate View

extension SPCompletedRideVc:GMSMapViewDelegate
{
    func setUpPinOnMap(lat:Double,long:Double,type:Int)
    {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if type == 0
        {
            // marker.icon = #imageLiteral(resourceName: "ic_star_location")
            let vw = Bundle.main.loadNibNamed("DestinationInfoView", owner: self, options: nil)?[0] as? DestinationInfoView
            vw?.lblDestinationName.text = mapping.string(forKey: "Your_location_key")
            marker.icon = #imageLiteral(resourceName: "ic_towig_service_icon_round_map")
            marker.iconView = vw
            marker.userData = "1"
        }
        else
        {
            let vw = Bundle.main.loadNibNamed("DestinationInfoView", owner: self, options: nil)?[0] as? DestinationInfoView
            vw?.lblDestinationName.text = strDestinationAddress
            marker.icon = #imageLiteral(resourceName: "ic_end_location")
            marker.iconView = vw
            marker.userData = "2"
        }
        
        marker.map = viewOfGoogleMaps
        marker.tracksViewChanges  = true
        marker.tracksInfoWindowChanges = true
        
        // self.viewOfGoogleMaps.selectedMarker = marker
        self.viewOfGoogleMaps.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 15)
        
    }
    
}





