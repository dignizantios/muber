//
//  SPHomeVc.swift
//  Muber
//
//  Created by YASH on 23/03/18.
//

import UIKit
import GoogleMaps

class SPHomeVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var btnHeader: UIButton!
    
    @IBOutlet weak var btnSwitchOffOn: UIButton!
    
    @IBOutlet weak var vwOfGoogleMap: GMSMapView!
    
    //MARK: - Variable
    
    let revealController = SWRevealViewController()
    
    var isTappedONOffBtn = Bool()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        self.vwOfGoogleMap.delegate = self
        setupGoogleMaps(view: vwOfGoogleMap)
        setUpPinOnMap(lat: userCurrentLocation?.coordinate.latitude ?? 0.0, long: userCurrentLocation?.coordinate.longitude ?? 0.0)
        
        
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        btnHeader.addTarget(self.revealViewController, action: #selector(revealController.revealToggle(_:)), for: .touchUpInside)
        
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        revealController.panGestureRecognizer()
//        revealController.tapGestureRecognizer()
//    }
    
    @IBAction func btnSwitchOffOnTapped(_ sender: UIButton)
    {
        
        if isTappedONOffBtn
        {
            btnSwitchOffOn.isSelected = false
            self.isTappedONOffBtn = false
            self.lblHeader.text = mapping.string(forKey: "OFFLINE_key")
        }
        else
        {
            btnSwitchOffOn.isSelected = true
            self.isTappedONOffBtn = true
            self.lblHeader.text = mapping.string(forKey: "ONLINE_key")
        }
        
        let vc = SPStoryboard.instantiateViewController(withIdentifier: "SPStartNavigationVc") as! SPStartNavigationVc
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension SPHomeVc
{
    
    func setupUI()
    {
        
        self.lblHeader.backgroundColor = MySingleton.sharedManager.themeNaviColor
        self.lblHeader.textColor = .white
        self.lblHeader.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
        self.lblHeader.text = mapping.string(forKey: "OFFLINE_key")
        
    }
    
}


//MARK:- Google Delegate View

extension SPHomeVc:GMSMapViewDelegate
{
    func setUpPinOnMap(lat:Double,long:Double)
    {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.icon = #imageLiteral(resourceName: "ic_car_map_image")
        marker.map = vwOfGoogleMap
        marker.tracksViewChanges  = true
        marker.tracksInfoWindowChanges = true
        
        // self.viewOfGoogleMaps.selectedMarker = marker
        self.vwOfGoogleMap.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 15)
        
    }
    
/*    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        if let view = marker.userData as? String
        {
            if view == "1"
            {
                let vw = Bundle.main.loadNibNamed("SourceInfoView", owner: self, options: nil)?[0] as? SourceInfoView
                return vw
            }
        }
        let vw = Bundle.main.loadNibNamed("DestinationInfoView", owner: self, options: nil)?[0] as? DestinationInfoView
        return vw
    }
*/
    // mapview
}

