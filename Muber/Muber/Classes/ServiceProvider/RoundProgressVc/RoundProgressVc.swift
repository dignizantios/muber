//
//  RoundProgressVc.swift
//  Muber
//
//  Created by YASH on 02/04/18.
//

import UIKit
import KDCircularProgress
import GoogleMaps



class RoundProgressVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var btnImage: UIButton!
    
    @IBOutlet weak var progressBar: KDCircularProgress!
    
    @IBOutlet weak var viewOfGoogleMap: GMSMapView!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    //MARK: - Variable
    
    var time = 120
    var zero = 0
    
    var timer = Timer()
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.viewOfGoogleMap.delegate = self
        setupGoogleMaps(view: viewOfGoogleMap)
        self.viewOfGoogleMap.isUserInteractionEnabled = false
        
        setUpPinOnMap(lat: userCurrentLocation?.coordinate.latitude ?? 0.0, long: userCurrentLocation?.coordinate.longitude ?? 0.0)
        
        setupProgressBar()
        
//        self.lblAddress.text =
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(repeatRound), userInfo: nil, repeats: true)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        
        print("Width : \(self.viewOfGoogleMap.layer.bounds.width)")
        print("Height : \(self.viewOfGoogleMap.layer.bounds.height)")
        
        self.viewOfGoogleMap.layer.cornerRadius = (self.viewOfGoogleMap.layer.bounds.width/2)
        self.viewOfGoogleMap.layer.masksToBounds = true
        
    }
    
    @objc func repeatRound()
    {
        /*
        zero += 1
        
        self.progressBar.angle = Double(CGFloat(self.zero))
        //        self.progress.value = CGFloat(self.zero)
        
        
        if zero == time
        {
            timer.invalidate()
        }
        */
    }
    
    
}


extension RoundProgressVc
{
    
    //MARK: - Progressbar setup
    
    func setupProgressBar()
    {
        progressBar.progressInsideFillColor = .clear
        
        progressBar.startAngle = Double(kstartAngel)
        progressBar.progressColors = [kProgressColor,kProgressColor,kProgressColor]
        progressBar.progressInsideFillColor = .clear
        progressBar.clockwise = kClockwise
        progressBar.trackColor = kTrackColor
        progressBar.trackThickness = CGFloat(kProgressThikness)
        progressBar.progressThickness = CGFloat(kProgressTrackThikness)
        
    }
    
    
}


extension RoundProgressVc:GMSMapViewDelegate
{
    func setUpPinOnMap(lat:Double,long:Double)
    {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.icon = #imageLiteral(resourceName: "ic_user_ride_request")
        marker.map = viewOfGoogleMap
        self.viewOfGoogleMap.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 15)
    }
    
}


