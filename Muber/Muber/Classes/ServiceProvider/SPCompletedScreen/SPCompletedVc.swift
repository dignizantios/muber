//
//  SPCompletedVc.swift
//  Muber
//
//  Created by YASH on 27/03/18.
//

import UIKit

class SPCompletedVc: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblInfoRamtion: UILabel!
    
    @IBOutlet weak var btnOk: CustomButton!
    
    //MARK: - Variable
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBarWithBackButton(titleText: mapping.string(forKey: "COMPLETED_key"))
    }

}


extension SPCompletedVc
{
    
    //MARK: - SetupUI
    
    func setupUI()
    {
        
        self.lblInfoRamtion.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 18)
        self.lblInfoRamtion.textColor = MySingleton.sharedManager.themeGrayFontColor
        
        btnOk.setTitle(mapping.string(forKey: "OK_key"), for: .normal)
        btnOk.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnOk.setTitleColor(.white, for: .normal)
        btnOk.backgroundColor = MySingleton.sharedManager.themeRedColor
        
    }
    
    
    //MARK: - IBAction
    
    @IBAction func btnOKTapped(_ sender: UIButton)
    {
        
        
    }
}


