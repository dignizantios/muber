//
//  SPStartNavigationVc.swift
//  Muber
//
//  Created by YASH on 03/04/18.
//

import UIKit
import GoogleMaps

class SPStartNavigationVc: UIViewController {

    //MARK:- outlet
    
    @IBOutlet weak var btnUpperNavigation: UIButton!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var viewOfGoogleMap: GMSMapView!
    
    @IBOutlet weak var imgUser: CustomImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblRating: UILabel!
    
     @IBOutlet weak var btnSideUserInfo: UIButton!
    
    //MARK: - variable
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setUpPinOnMap(lat: userCurrentLocation?.coordinate.latitude ?? 0.0, long: userCurrentLocation?.coordinate.longitude ?? 0.0, type: 0)
        
        setUpPinOnMap(lat: destinationLocation?.coordinate.latitude ?? 0.0, long: destinationLocation?.coordinate.longitude ?? 0.0, type: 1)
        
        self.setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBarWithBackButton(titleText: "Time")
    }
    
    
    @IBAction func btnUpperNavigationTapped(_ sender: UIButton)
    {
        let vc = SPStoryboard.instantiateViewController(withIdentifier: "SPOntheWay") as! SPOntheWay
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
}


extension SPStartNavigationVc
{

    //MARK: - setup UI
    
    func setupUI()
    {
        self.viewOfGoogleMap.delegate = self
        setupGoogleMaps(view: viewOfGoogleMap)
        
        self.lblName.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.lblName.textColor = .black
        
        self.lblRating.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
        self.lblRating.textColor = MySingleton.sharedManager.themeStatusColor
        
        btnUpperNavigation.titleLabel?.text = mapping.string(forKey: "Navigate_key")
        btnUpperNavigation.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 16)
        btnUpperNavigation.setTitleColor(.black, for: .normal)
        
        lblAddress.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 20)
        lblAddress.textColor = MySingleton.sharedManager.themeGrayFontColor
        
    }
    
    //MARK:- IBAtion method
    
    @IBAction func btnSideUserInfoTapped(_ sender: UIButton)
    {
        
        let obj = DriverInformation()
        obj.isComeFromSP = "0"
        obj.modalPresentationStyle = .overCurrentContext
        self.present(obj, animated: false, completion: nil)
        
    }
    
}


extension SPStartNavigationVc:GMSMapViewDelegate
{
    func setUpPinOnMap(lat:Double,long:Double,type:Int)
    {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if type == 0
        {
            let vw = Bundle.main.loadNibNamed("DestinationInfoView", owner: self, options: nil)?[0] as? DestinationInfoView
            vw?.lblDestinationName.text = mapping.string(forKey: "Your_location_key")
            marker.iconView = vw
            marker.icon = #imageLiteral(resourceName: "ic_towig_service_icon_round_map")
        }
        else
        {
            marker.icon = #imageLiteral(resourceName: "ic_star_location")
        }
        marker.map = viewOfGoogleMap
        self.viewOfGoogleMap.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 15)
    }
    
}


