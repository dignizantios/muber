//
//  HeightWeightApprox.swift
//  Muber
//
//  Created by Jaydeep on 03/04/18.
//

import UIKit

protocol delegateApproxHeight {
    func setValue(load:String,height:String)
}

class HeightWeightApprox: UIViewController {

    //MARK:- Variable Declaration
    
    var deleApproxHeight : delegateApproxHeight?
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblVehicleLoad: UILabel!
    @IBOutlet weak var txtVehicleLoad: CustomTextField!
    @IBOutlet weak var lblVehicleHeight: UILabel!
    @IBOutlet weak var txtVehicleHeight: CustomTextField!
    @IBOutlet weak var btnDoneOutlet: CustomButton!
    
    //MARK:- ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
       
    }
    
    //MARK:- Setup UI
    
    func setupUI()
    {
        lblTitle.text = mapping.string(forKey: "Height_weight_approx_key")
        lblTitle.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 16)
        lblTitle.textColor = MySingleton.sharedManager.themeLightTextColor
        
        [txtVehicleLoad,txtVehicleHeight].forEach { (txtfield) in
            txtfield?.textColor = MySingleton.sharedManager.themeLightTextColor
            txtfield?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 14)
            txtfield?.placeholder = mapping.string(forKey: "Select_here_key")
        }
        
        [lblVehicleLoad,lblVehicleHeight].forEach { (lbl) in
            lbl?.textColor = .black
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 16)
        }
        lblVehicleLoad.text = mapping.string(forKey: "VEHICLE_LOAD_APPROX_key")
        lblVehicleHeight.text = mapping.string(forKey: "HEIGH_APPROX_key")
        
        self.btnDoneOutlet.setTitle(mapping.string(forKey: "DONE_key"), for: .normal)
        self.btnDoneOutlet.setTitleColor(.white, for: .normal)
        self.btnDoneOutlet.backgroundColor = MySingleton.sharedManager.themeRedColor
    }

    //MARK:- Action Zone
    
    @IBAction func btnCloseAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoneAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
        self.deleApproxHeight?.setValue(load: "0", height: "0")
    }
    
}
