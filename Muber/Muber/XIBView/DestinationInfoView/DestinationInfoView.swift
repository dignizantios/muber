//
//  DestinationInfoView.swift
//  Muber
//
//  Created by Jaydeep on 28/03/18.
//

import UIKit

class DestinationInfoView: UIView {

   //MARK:- Outlet Zone

    @IBOutlet weak var lblDestinationName: UILabel!
    
    //MARK:- ViewLifeCycle
    
    override func awakeFromNib() {
        [lblDestinationName].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
            lbl?.textColor = MySingleton.sharedManager.themeLightTextColor
        }
    }
}
