//
//  DriverInformation.swift
//  Muber
//
//  Created by Jaydeep on 29/03/18.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class DriverInformation: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrDriveInfo:[JSON] = []
    
    var arraySPSideInfo:[JSON] = []
    
    
    var isComeFromSP = String()
    /*
     isComeFromSP= "0" For SP
     isComeFromSP = "" FOR User provider side
     
    */
    
     //MARK:- Outlet Zone
    
    
    @IBOutlet weak var leadingOfRear: NSLayoutConstraint! 
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblInfo: UITableView!
    

    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.leadingOfRear.constant = -self.view.frame.width
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       presentViewAnimation(interval: 1)
    }
    
    //MARK:- Private Method
    func presentViewAnimation(interval:TimeInterval) {
        UIView.animate(withDuration: interval) {
            self.leadingOfRear.constant = 0
            self.view.layoutIfNeeded()
        }        
      
    }
    
    //MARK:- Setup UI
    
    func setupUI()
    {
        lblTitle?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 20)
        lblTitle?.textColor = MySingleton.sharedManager.themeLightTextColor
        
        if isComeFromSP == "0"
        {
            lblTitle.text = "\(mapping.string(forKey: "ATTENTION_key")!) \n \(mapping.string(forKey: "you_are_almost_arrive_to_your_client_key")!)"
        }
        else
        {
            lblTitle.text = mapping.string(forKey: "Please_turn_on_your_emergency_light_your_assistance_are_close_to_your_key")
        }
        
        self.tblInfo.register(UINib(nibName: "DriverCancelCell", bundle: nil), forCellReuseIdentifier: "DriverCancelCell")
        self.tblInfo.register(UINib(nibName: "DriveInfoCell", bundle: nil), forCellReuseIdentifier: "DriveInfoCell")
        
        if isComeFromSP == "0"
        {
            setupSPSideInfo()
        }
        else
        {
            setupDriveInfoArray()
        }
        
    }
    
    //MARK:- Setup Array
    
    func setupDriveInfoArray()
    {
        arrDriveInfo = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Asistence_Information_key"))
        dict["selected"] = "0"
        self.arrDriveInfo.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Cancel_the_Asistence_key"))
        dict["selected"] = "0"
        self.arrDriveInfo.append(dict)
        
        self.tblInfo.reloadData()
    }
    
    
    func setupSPSideInfo()
    {
        arrDriveInfo = []
        
        var dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Client_Information_key"))
        dict["selected"] = "0"
        self.arrDriveInfo.append(dict)
        
        dict = JSON()
        dict["name"] = JSON(mapping.string(forKey: "Vehicle_Information_key"))
        dict["selected"] = "0"
        self.arrDriveInfo.append(dict)
        
        self.tblInfo.reloadData()
    }
    

    //MARK:- Action Zone
    
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func btnMessageAction()
    {
        if isComeFromSP == "0"
        {
            //SP side
        }
        else
        {
            
        }
    }
    
    @objc func btnCallAction()
    {
        if isComeFromSP == "0"
        {
            //SP side
        }
        else
        {
            
        }
    }
    
    @objc func btnNoAction()
    {
        
    }
    
    @objc func btnYesAction()
    {
        
    }

}

extension DriverInformation:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDriveInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DriveInfoCell") as! DriveInfoCell
            
            let dict = arrDriveInfo[indexPath.row]
            
            cell.btnMessageOutlet.addTarget(self, action: #selector(btnMessageAction), for: .touchUpInside)
            cell.btnCallOutlet.addTarget(self, action: #selector(btnCallAction), for: .touchUpInside)
            
            if isComeFromSP == "0"
            {
                cell.lblDriverName.text = "\(mapping.string(forKey: "Name_key")!) : "
//                cell.lblID.text = "\(mapping.string(forKey: "ID_key")) : "
                cell.lblPhone.text = "\(mapping.string(forKey: "Phone_key")!) : "
//                cell.lblLiecenseName.text = "\(mapping.string(forKey: "License_key")) : "
                cell.lblName.text = dict["name"].stringValue
                cell.viewOfId.isHidden = true
                cell.viewOfLiecense.isHidden = true
                
                
                cell.viewOfcall.isHidden = false
                cell.viewOfMessage.isHidden = false
                
                if dict["selected"].stringValue == "0"
                {
                    cell.imgOfCollapse.image = #imageLiteral(resourceName: "ic_drop_down_gray")
                    cell.heightOfView.constant = 0
                    cell.viewOfBottom.isHidden = true
                    self.tblInfo.separatorColor = .clear
                }
                else{
                    cell.imgOfCollapse.image = #imageLiteral(resourceName: "ic_drop_up_gray")
                    cell.heightOfView.constant = 180
                    cell.viewOfBottom.isHidden = false
                    self.tblInfo.separatorColor = MySingleton.sharedManager.themeGrayHeaderColor
                }
            }
            else
            {
                cell.lblDriverName.text = "\(mapping.string(forKey: "Name_key")!) : "
                cell.lblID.text = "\(mapping.string(forKey: "ID_key")!) : "
                cell.lblPhone.text = "\(mapping.string(forKey: "Phone_key")!) : "
                cell.lblLiecenseName.text = "\(mapping.string(forKey: "License_key")!) : "
                cell.lblName.text = dict["name"].stringValue
                
                
                if dict["selected"].stringValue == "0"
                {
                    cell.imgOfCollapse.image = #imageLiteral(resourceName: "ic_drop_down_gray")
                    cell.heightOfView.constant = 0
                    cell.viewOfBottom.isHidden = true
                    self.tblInfo.separatorColor = .clear
                }
                else{
                    cell.imgOfCollapse.image = #imageLiteral(resourceName: "ic_drop_up_gray")
                    cell.heightOfView.constant = 250
                    cell.viewOfBottom.isHidden = false
                    self.tblInfo.separatorColor = MySingleton.sharedManager.themeGrayHeaderColor
                }
                
            }
            
            
            return cell
        }
        
        
        if isComeFromSP == "0"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DriveInfoCell") as! DriveInfoCell
            
            let dict = arrDriveInfo[indexPath.row]
            
            cell.lblDriverName.text = "\(mapping.string(forKey: "Brand_key")!) : "
            //                cell.lblID.text = "\(mapping.string(forKey: "ID_key")) : "
            cell.lblPhone.text = "\(mapping.string(forKey: "Year_key")!) : "
            //                cell.lblLiecenseName.text = "\(mapping.string(forKey: "License_key")) : "
            cell.lblName.text = dict["name"].stringValue
            
            
            cell.viewOfId.isHidden = true
            cell.viewOfLiecense.isHidden = true
            cell.viewOfcall.isHidden = true
            cell.viewOfMessage.isHidden = true
            
            
            if dict["selected"].stringValue == "0"
            {
                cell.imgOfCollapse.image = #imageLiteral(resourceName: "ic_drop_down_gray")
                cell.heightOfView.constant = 0
                cell.viewOfBottom.isHidden = true
                self.tblInfo.separatorColor = .clear
            }
            else{
                cell.imgOfCollapse.image = #imageLiteral(resourceName: "ic_drop_up_gray")
                cell.heightOfView.constant = 70
                cell.viewOfBottom.isHidden = false
                self.tblInfo.separatorColor = MySingleton.sharedManager.themeGrayHeaderColor
            }
            
            return cell
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DriverCancelCell") as! DriverCancelCell
            let dict = arrDriveInfo[indexPath.row]
            
            cell.lblCancelName.text = dict["name"].stringValue
            cell.btnYesOutlet.addTarget(self, action: #selector(btnYesAction), for: .touchUpInside)
            cell.btnNoOutlet.addTarget(self, action: #selector(btnNoAction), for: .touchUpInside)
            
            if dict["selected"].stringValue == "0"
            {
                cell.imgOfCollapse.image = #imageLiteral(resourceName: "ic_drop_down_gray")
                cell.heightOfView.constant = 0
                cell.viewOfBottom.isHidden = true
                self.tblInfo.separatorColor = .clear
            }
            else{
                cell.imgOfCollapse.image = #imageLiteral(resourceName: "ic_drop_up_gray")
                cell.heightOfView.constant = 140
                cell.viewOfBottom.isHidden = false
                self.tblInfo.separatorColor = MySingleton.sharedManager.themeGrayHeaderColor
            }
            return cell
        }
        
       
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dict = arrDriveInfo[indexPath.row]
        if dict["selected"].stringValue == "0"
        {
            dict["selected"] = "1"
        }
        else
        {
            dict["selected"] = "0"
        }
        self.arrDriveInfo[indexPath.row] = dict
        self.tblInfo.reloadRows(at: [indexPath], with: .automatic)
    }
}
