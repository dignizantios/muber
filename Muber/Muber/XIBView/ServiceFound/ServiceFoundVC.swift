//
//  ServiceFoundVC.swift
//  Muber
//
//  Created by Jaydeep on 29/03/18.
//

import UIKit
import Pulsator

public protocol CommonServiceDelegate
{
    func setName(strType:String)
}

class ServiceFoundVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    let pulsator = Pulsator()
  //  var isComeFromRouteSearch = Bool()
    var ServiceDelegate : CommonServiceDelegate?
    var SearchStatus = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgOfTitle: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var heightOfBottomView: NSLayoutConstraint!
    @IBOutlet weak var imgServiceFound: UIImageView!
    
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.heightOfBottomView.constant = 100
        
        [lblTitle].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
            lbl?.textColor = .black
        }
        
        imgServiceFound.layer.superlayer?.insertSublayer(pulsator, below: imgServiceFound.layer)
        
        pulsator.numPulse = kPluseNum
        pulsator.radius = kPulseRadius
        pulsator.borderColor = kPulseBorderColor
        pulsator.animationDuration = kPulseAnimationDuration
        pulsator.backgroundColor = kPulseBackgroundColor
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if SearchStatus == "0"
        {
            self.imgOfTitle.image = #imageLiteral(resourceName: "ic_calculate_route")
            self.lblTitle.text =  mapping.string(forKey: "Calculating_Route_key")
        }
        else if SearchStatus == "1"
        {
            self.imgOfTitle.image = #imageLiteral(resourceName: "ic_searching_for_service")
            self.lblTitle.text =  mapping.string(forKey: "Searching_for_near_by_device_key")
        }
        else
        {
            self.imgOfTitle.image = #imageLiteral(resourceName: "ic_searching_for_service")
            self.lblTitle.text =  mapping.string(forKey: "Service_found_key")
        }
       
        setupIntilizeView()
       
    }
    
    //MARK:- Setup UI
    
    func setupIntilizeView()
    {
       
        pulsator.start()
        
        view.layer.layoutIfNeeded()
        pulsator.position = imgServiceFound.layer.position
        
        if SearchStatus == "0"
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                self.serviceFound()
            })
        }
        else if SearchStatus == "1"
        {
            self.imgOfTitle.image = #imageLiteral(resourceName: "ic_searching_for_service")
            self.lblTitle.text =  mapping.string(forKey: "Searching_for_near_by_device_key")
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                self.dismiss(animated: false, completion: nil)
                self.ServiceDelegate?.setName(strType: self.SearchStatus)
            })
        }
        else
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                self.dismiss(animated: false, completion: nil)
                self.ServiceDelegate?.setName(strType: self.SearchStatus)
            })
        }
        
    }
    
    //MARK:- Service find
    
    func serviceFound()
    {
        UIView.animate(withDuration: 0.5, animations: {
           // self.heightOfBottomView.constant = 100
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
            self.dismiss(animated: false, completion: nil)
            self.ServiceDelegate?.setName(strType: self.SearchStatus)
        })
    }
    
    

}
