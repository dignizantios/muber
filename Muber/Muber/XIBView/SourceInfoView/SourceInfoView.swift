//
//  SourceInfoView.swift
//  Muber
//
//  Created by Jaydeep on 28/03/18.
//

import UIKit

class SourceInfoView: UIView {

    //MARK:- Outlet Zone

    @IBOutlet weak var lblRemainingTime: UILabel!
    @IBOutlet weak var lblSourcesName: UILabel!
    
    override func awakeFromNib() {
        [lblRemainingTime,lblSourcesName].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
            lbl?.textColor = MySingleton.sharedManager.themeLightTextColor
        }
    }
}
