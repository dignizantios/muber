//
//  CommonPickerVC.swift
//  Muber
//
//  Created by Jaydeep on 26/03/18.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import KSToastView

protocol CommonPickerDelegate
{
    func setValuePicker(value : String)
}


class CommonPickerVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet var pickerVw: UIPickerView!
    
    @IBOutlet var btnDone: UIButton!
    
    @IBOutlet var btnCancel: UIButton!
    
    @IBOutlet var vwBtns: UIView!
    
    //MARK: - Variable
    
    var strValueStore = String()
    
    var arrayPicker: [JSON] = []
    
    var pickerDelegate : CommonPickerDelegate?
    
    var selectedYear = String()
    var selectedMonth = String()
    
    var isComeFromExpiryMonth = Bool()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Array : \(self.arrayPicker)")
        vwBtns.backgroundColor = MySingleton.sharedManager.themeNaviColor
        
        btnDone.setTitle(mapping.string(forKey: "DONE_key"), for: .normal)
        btnDone.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 18)
        btnDone.setTitleColor(.white, for: .normal)
        
        btnCancel.setTitle(mapping.string(forKey: "CANCEL_key"), for: .normal)
        btnCancel.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 18)
        btnCancel.setTitleColor(.white, for: .normal)
        
    }
    
}

extension CommonPickerVC
{

    //MARK: - IBAction
    
    @IBAction func btnCancelTapped(sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnDoneTapped(sender: UIButton)
    {

        let dict = arrayPicker[self.pickerVw.selectedRow(inComponent: 0)]
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy"
        let strYear = dateformatter.string(from: Date())
        print("strYear : \(strYear)")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM"
        let strMonth = formatter.string(from: Date())
        print("strMonth : \(strMonth)")
        
        if isComeFromExpiryMonth
        {
            print("SelectedYear : \(dict["value"].stringValue)")
            print("SelectedMonth : \(selectedMonth)")
            
            if Int(selectedYear) == Int(strYear)
            {
                if Int(dict["value"].stringValue)! <= Int(strMonth)!
                {
                    toastMessage(title: mapping.string(forKey: "Please_select_valid_expiry_month_key"))
                    return
                }
            }
            
        }
        else
        {
            print("SelectedYear : \(dict["value"].stringValue)")
            print("SelectedMonth : \(selectedMonth)")
            
            if Int(dict["value"].stringValue) == Int(strYear)
            {
                if Int(selectedMonth)! <= Int(strMonth)!
                {
                    toastMessage(title: mapping.string(forKey: "Please_select_valid_expiry_year_key"))
                    return
                }
            }
            
        }
       
        
        self.dismiss(animated: true, completion: nil)
        print("Store : \(strValueStore)")
//        let dict = arrayPicker[self.pickerVw.selectedRow(inComponent: 0)]
        pickerDelegate?.setValuePicker(value: dict["value"].stringValue)
        
    }
    
}

extension CommonPickerVC: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayPicker[row]["value"].stringValue
    }
 
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        strValueStore = arrayPicker[row]["value"].stringValue
    }
    
    
}






