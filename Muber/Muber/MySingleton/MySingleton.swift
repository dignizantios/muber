//
//  MySingleton.swift
//  Accounting House
//
//  Created by PC on 19/12/17.
//  Copyright © 2017 PC. All rights reserved.
//

import Foundation
import UIKit

class MySingleton
{
    // Declare class instance property
    static let sharedManager = MySingleton()
    
    //MARK: - Application color theme declaration
    var themeNaviColor = UIColor()
    var themeRedColor = UIColor()
    var themeLightTextColor = UIColor()
    var themeGrayFontColor = UIColor()
    var themeGreenFontColor = UIColor()
    var themeStatusColor = UIColor()
    var themeGrayHeaderColor = UIColor()
    
    
    
    //MARK: - UIScreen Bounds
    var screenRect = CGRect()
    
    //MARK: - Initializer of Class
    // @desc Declare an initializer
    // Because this class is singleton only one instance of this class can be created
    /// Initialization of variables with default value
    init()
    {
        themeNaviColor = UIColor.init(red: 30/255.0, green: 37/255.0, blue: 45/255.0, alpha: 1.0)
        themeRedColor = UIColor.init(red: 239/255.0, green: 50/255.0, blue: 37/255.0, alpha: 1.0)
        themeLightTextColor = UIColor.init(red: 204/255.0, green: 204/255.0, blue: 204/255.0, alpha: 1.0)
        themeGrayFontColor = UIColor.init(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 1.0)
        themeGreenFontColor = UIColor.init(red: 73/255.0, green: 194/255.0, blue: 57/255.0, alpha: 1.0)
        themeStatusColor = UIColor.init(red: 33/255.0, green: 105/255.0, blue: 149/255.0, alpha: 1.0)
        themeGrayHeaderColor = UIColor.init(red: 153/255.0, green: 153/255.0, blue: 153/255.0, alpha: 1.0)
        
        screenRect = UIScreen.main.bounds
    }
    
    //MARK: - Get Custom Fonts With size
    
    //MARK: - Black color
    func getFontForTypeLatoBlackWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-Black", size: CGFloat(fontSize))!
    }
    
    func getFontForTypeLatoBlackItalicWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-BlackItalic", size: CGFloat(fontSize))!
    }
    
    //MARK: - Lato Bold
    
    
    func getFontForTypeSemiBoldWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-Semibold", size: CGFloat(fontSize))!
    }
    
    func getFontForTypeBoldWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-Bold", size: CGFloat(fontSize))!
    }
    
    func getFontForTypeBoldItalicWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-BoldItalic", size: CGFloat(fontSize))!
    }
    
    //MARK: - Lato HairLine
    
    func getFontForTypeHairlineWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-Hairline", size: CGFloat(fontSize))!
    }
    
    func getFontForTypeBoldHairlineItalicWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-HairlineItalic", size: CGFloat(fontSize))!
    }
    
    //MARK: - Lato Italic
    
    func getFontForTypeItalicWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-Italic", size: CGFloat(fontSize))!
    }
    
    //MARK: - Lato Light
    
    func getFontForTypeLightWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-Light", size: CGFloat(fontSize))!
    }
    
    func getFontForTypeLightItalicWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-LightItalic", size: CGFloat(fontSize))!
    }
    
    //MARK: - Regular
    
    func getFontForTypeRegularWithSize(fontSize : Int) -> UIFont
    {
        return UIFont(name: "Lato-Regular", size: CGFloat(fontSize))!
    }
    
}
