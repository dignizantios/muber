//
//  ViewController.swift
//  Muber
//
//  Created by Jaydeep on 22/03/18.
//

import UIKit
import CarbonKit
import LinearProgressBarMaterial
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class ViewController: UIViewController,SWRevealViewControllerDelegate {
    
    //MARK: - Outlet
    
    @IBOutlet weak var btnNaviLogin: UIButton!
    @IBOutlet weak var vwBottomLogin: UIView!
    @IBOutlet weak var btnNaviCreateAccount: UIButton!
    @IBOutlet weak var vwBottomCreate: UIView!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var txtMobile: CustomTextField!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnLoginBottom: UIButton!
    @IBOutlet weak var vwProgress: UIView!
    @IBOutlet weak var btnFPPasswordOutlet: UIButton!
    
    //MARK: - Variable
   

    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        linearBar = LinearProgressBar(frame: CGRect(x: 0, y: 0, width: self.vwProgress.frame.size.width, height: 5))
        configureLinearProgressBarView(linearBar : linearBar)
        self.vwProgress.addSubview(linearBar)
        
        self.setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:-Action zone
    
    @IBAction func btnLoginBottomTapped(_ sender: UIButton)
    {
        if ((self.txtMobile.text?.trimmingCharacters(in: .whitespaces))?.isEmpty)!
        {
            toastMessage(title: mapping.string(forKey: "Please_enter_mobile_number_key"))
        }
        else if (self.txtMobile.text?.count)! < 10
        {
            toastMessage(title: mapping.string(forKey: "Please_enter_valid_mobile_number_key"))
        }
        else if (self.txtPassword.text?.isEmpty)!
        {
            toastMessage(title: mapping.string(forKey: "Please_enter_password_key"))
        }
        else if !(self.txtPassword.text == "")
        {
            if (self.txtPassword.text?.count)! < 6
            {
                toastMessage(title: mapping.string(forKey: "Password_must_be_at_least_6_characterslong_key"))
            }
            else
            {
                UserLogin()
            }
        }
    }
    
    @IBAction func btnFPPasswordAction(_ sender: Any)
    {
        let register = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(register, animated: true)
    }
    
}

extension ViewController
{

    //MARK: - Setup
    
    func setupUI()
    {
        //upper button
        
        btnNaviLogin.setTitle(mapping.string(forKey: "Login_key"), for: .normal)
        btnNaviCreateAccount.setTitle(mapping.string(forKey: "Creat_an_account_key"), for: .normal)
        
        btnNaviLogin.setTitleColor(.black, for: .normal)
        btnNaviLogin.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 25)
        self.vwBottomLogin.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        
        btnNaviCreateAccount.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 25)
        btnNaviCreateAccount.setTitleColor(MySingleton.sharedManager.themeGrayFontColor, for: .normal)
        
        self.vwBottomCreate.backgroundColor = MySingleton.sharedManager.themeGrayFontColor
        
        //text and bottom button
        
        btnLoginBottom.setTitle(mapping.string(forKey: "LOGIN_key"), for: .normal)
        btnLoginBottom.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 22)
        btnLoginBottom.setTitleColor(.white, for: .normal)
        btnLoginBottom.backgroundColor = MySingleton.sharedManager.themeRedColor
        
        [lblMobile,lblPassword].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 20)
            lbl?.textColor = .black
        }
        
        lblMobile.text = mapping.string(forKey: "MOBILE_key")
        lblPassword.text = mapping.string(forKey: "PASSWORD_key")
        
        [txtMobile,txtPassword].forEach { (txt) in
            txt?.placeholder = mapping.string(forKey: "Enter_here_key")
            txt?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 18)
            txt?.placeHolderColor = MySingleton.sharedManager.themeGrayFontColor
            txt?.delegate = self
            
        }
        addDoneButtonOnKeyboard(textfield: txtMobile)
        
        btnFPPasswordOutlet.setTitle(mapping.string(forKey: "Btn_Forgot_Password_key"), for: .normal)
        btnFPPasswordOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 18)
        btnFPPasswordOutlet.setTitleColor(MySingleton.sharedManager.themeGrayFontColor, for: .normal)
    }
    
    
    @IBAction func btnNaviLoginTapped(_ sender: UIButton)
    {
//        let register = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC//
//        self.navigationController?.pushViewController(register, animated: false)
    }
    
    @IBAction func btnNaviCreateTapped(_ sender: UIButton)
    {
        let register = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(register, animated: false)
    }
    
}

extension ViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension ViewController
{
    func UserLogin()
    {
        let kRegiURL = "\(MuberURL)\(userLogin)"
        print("URL:- \(kRegiURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param =  ["lang": isSpanish,
                          "mobile": self.txtMobile.text ?? "",
                          "password": self.txtPassword.text ?? "",
                          "timezone":timeZoneIdentifiers]
            
            print("parm \(param)")
            //  show
            linearBar.startAnimation()
            
            CommonService().Service(url: kRegiURL, param: param, completion: { (respones) in
                linearBar.stopAnimation()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == "1"
                    {                        
                        if json["data"]["is_mobile_verified"].stringValue == "0"
                        {
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                            vc.dictData = json["data"]
                            vc.isComeFromFP = false
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else
                        {
                            let data = json["data"]
                            guard let rowdata = try? data.rawData() else {return}
                            Defaults.setValue(rowdata, forKey: "userDetail")
                            Defaults.synchronize()
                        }
                    }
                    else
                    {
                        toastMessage(title: json["msg"].stringValue)
                    }
                }
                else{
                    toastMessage(title: mapping.string(forKey: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            toastMessage(title: mapping.string(forKey: "No_internet_connection,_please_try_again_later_key"))
        }
    }
}

