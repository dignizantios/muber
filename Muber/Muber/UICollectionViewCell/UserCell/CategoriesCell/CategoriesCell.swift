//
//  CategoriesCell.swift
//  Muber
//
//  Created by Jaydeep on 23/03/18.
//

import UIKit

class CategoriesCell: UICollectionViewCell {
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgOfCategories: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var viewOfHorizontal: UIView!
    @IBOutlet weak var viewOfVerticle: UIView!
    
    //MARK:- ViewLife Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblCategoryName.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        self.lblCategoryName.textColor = UIColor.black
    }

}
