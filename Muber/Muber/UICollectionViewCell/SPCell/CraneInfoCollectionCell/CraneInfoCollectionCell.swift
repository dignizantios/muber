//
//  CraneInfoCollectionCell.swift
//  Muber
//
//  Created by Jaydeep on 02/04/18.
//

import UIKit

class CraneInfoCollectionCell: UICollectionViewCell {

    
    //MARK: - Outlet
    
    @IBOutlet weak var btnUpper: UIButton!
    
    @IBOutlet weak var imgInfo: UIImageView!
    
    //MARK:- view life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
