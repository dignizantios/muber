//
//  Global.swift
//  Muber
//
//  Created by Jaydeep on 22/03/18.
//

import Foundation
import KSToastView
import CoreLocation
import KDCircularProgress
import LinearProgressBarMaterial

//MARK:- Variable Declaration

var Spanish = "sp"
var English = "en"
var mapping:StringMapping = StringMapping.shared()
let Defaults = UserDefaults.standard
var toastDuration:TimeInterval = 2.0
var userCurrentLocation:CLLocation?
var appDelegate = UIApplication.shared.delegate as! AppDelegate
var BasicUsername:String = "root"
var BasicPassword:String = "123"
var strLoader:String = ""
var LoaderType:Int = 29
let Loadersize = CGSize(width: 30, height: 30)
var strDeviceType:String = "1"
var isSpanish = String() // 0 = eng,1 = spanish

//MARK:- get timezone

var timeZoneIdentifiers: String { return TimeZone.current.identifier }

//MARK:-  Model Object Initialize

var objBookRide = BookRide()

//MARK:- lat,long place

var sourceLocation:CLLocation?
var strSourceAddress = String()
var destinationLocation:CLLocation?
var strDestinationAddress = String()

//MARK:- Pulse effect default variable

let kPluseNum:Int = 5
let kPulseRadius:CGFloat = 240.0
let kPulseBorderColor:CGColor = UIColor(red: 255/255,green: 255/255,blue: 255/255,alpha: 1).cgColor
let kPulseAnimationDuration:TimeInterval = 4
let kPulseBackgroundColor:CGColor = UIColor(red: 150/255,green: 150/255,blue: 150/255,alpha: 1).cgColor

//MARK:- Google maps key

let kGoogleMapsKey = "AIzaSyDnfJ3QnQpG8-fEWjZdXQam6SqsnGqSy14"

//MARK:- Google Maps Credential

// aliamar7033@gmail.com
// aliamar@123

//MARK:- Storyboard Outlet

let userStoryboard = UIStoryboard(name: "User", bundle: nil)
let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let SPStoryboard = UIStoryboard(name: "ServiceProvider", bundle: nil)

//MARK:- Setup Language

func setLangauge(language:String)
{
    if language == Spanish
    {
        Defaults.set(Spanish, forKey: "language")
        Defaults.set(1, forKey: "lang")
        Defaults.synchronize()
        isSpanish = "1"
    }
    else
    {
        Defaults.set(English, forKey: "language")
        Defaults.set(0, forKey: "lang")
        Defaults.synchronize()
        isSpanish = "0"
    }
    print("Language - ",Defaults.value(forKey: "language"));
    StringMapping.shared().setLanguage()
}

//MARK:- Setup Toast Message

func toastMessage(title:String)
{
    KSToastView.ks_showToast(title, duration: toastDuration)
}

//MARK: - Progressbar Setup

let kProgressTrackThikness = 0.15
let kProgressThikness = 0.15
let kTrackColor: UIColor = UIColor(red: 33/255.0, green: 105/255.0, blue: 149/255.0, alpha: 0.5)
let kProgressColor: UIColor = UIColor(red: 33/255.0, green: 61/255.0, blue: 82/255.0, alpha: 1.0)
let kstartAngel = -90
let kClockwise : Bool = true

//MARK: - Linear ProgressBar setup

var linearBar = LinearProgressBar()

func startLinearAnimation()
{
  //  linearBar = LinearProgressBar()
    configureLinearProgressBar()
    linearBar.startAnimation()
}


func configureLinearProgressBar()
{
    linearBar.backgroundColor = UIColor.white
    linearBar.progressBarColor = MySingleton.sharedManager.themeRedColor
    linearBar.heightForLinearBar = 5
}
func configureLinearProgressBarView(linearBar : LinearProgressBar)
{
    linearBar.backgroundColor = UIColor.white
    linearBar.progressBarColor = MySingleton.sharedManager.themeRedColor
    linearBar.heightForLinearBar = 5
}

func stopLinearAnimation()
{
    linearBar.stopAnimation()
}




