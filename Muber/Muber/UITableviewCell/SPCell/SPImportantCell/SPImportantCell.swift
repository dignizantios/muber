//
//  SPImportantCell.swift
//  Muber
//
//  Created by YASH on 26/03/18.
//

import UIKit

class SPImportantCell: UITableViewCell {
    
    
    @IBOutlet weak var lblDetails: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblDetails.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        self.lblDetails.textColor = MySingleton.sharedManager.themeLightTextColor
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
