//
//  SideMenuCell.swift
//  Muber
//
//  Created by YASH on 23/03/18.
//

import UIKit

class SideMenuCell: UITableViewCell {

    //MARK: - outlet
    
    
    @IBOutlet weak var vwWhite: UIView!
    
    @IBOutlet weak var imgSideMenu: UIImageView!
    
    @IBOutlet weak var lblSideMenu: UILabel!
    
    @IBOutlet weak var lblArrow: UIImageView!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblSideMenu.textColor = .white
        
        self.lblSideMenu.font = MySingleton.sharedManager.getFontForTypeBoldWithSize(fontSize: 21)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
