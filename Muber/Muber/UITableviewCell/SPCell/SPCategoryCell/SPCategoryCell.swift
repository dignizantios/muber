//
//  SPCategoryCell.swift
//  Muber
//
//  Created by YASH on 24/03/18.
//

import UIKit

class SPCategoryCell: UITableViewCell {

    //MARK: - Outlet
    
    
    @IBOutlet weak var lblOptionTitle: UILabel!
    
    @IBOutlet weak var btnCheckUncheck: UIButton!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblOptionTitle.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 19)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
