//
//  SPImportantHeaderCell.swift
//  Muber
//
//  Created by YASH on 26/03/18.
//

import UIKit

class SPImportantHeaderCell: UITableViewCell {

    
    @IBOutlet weak var lblHeader: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblHeader.font = MySingleton.sharedManager.getFontForTypeBoldWithSize(fontSize: 17)
//        getFontForTypeSemiBoldWithSize(fontSize: 17)
       
        
        self.lblHeader.textColor = MySingleton.sharedManager.themeGrayFontColor
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
