//
//  DriveInfoCell.swift
//  Muber
//
//  Created by Jaydeep on 29/03/18.
//

import UIKit

class DriveInfoCell: UITableViewCell {

    //MARK:-  Outlet Zone
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewOfName: UIView!
    @IBOutlet weak var imgOfCollapse: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var viewOfId: UIView!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var viewOfPhone: UIView!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var viewOfLiecense: UIView!
    @IBOutlet weak var lblLiecenseName: UILabel!
    @IBOutlet weak var btnCallOutlet: CustomButton!
    @IBOutlet weak var btnMessageOutlet: CustomButton!
    @IBOutlet weak var viewOfcall: UIView!
    @IBOutlet weak var viewOfMessage: UIView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    @IBOutlet weak var viewOfBottom: UIView!
    
    //MARK:- ViewLife cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        [lblDriverName,lblID,lblPhone,lblLiecenseName].forEach { (lbl) in
            lbl?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 15)
            lbl?.textColor = MySingleton.sharedManager.themeLightTextColor
        }
        
        lblName?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 19)
        lblName?.textColor = MySingleton.sharedManager.themeLightTextColor
        
        [btnCallOutlet,btnMessageOutlet].forEach { (btn) in
            btn?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
            btn?.setTitleColor(.white, for: .normal)
        }
        
        btnCallOutlet.backgroundColor = MySingleton.sharedManager.themeRedColor
        self.btnCallOutlet.setTitle(mapping.string(forKey: "CALL_key"), for: .normal)
    
        btnMessageOutlet.backgroundColor = MySingleton.sharedManager.themeNaviColor
        self.btnMessageOutlet.setTitle(mapping.string(forKey: "MESSAGE_key"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
