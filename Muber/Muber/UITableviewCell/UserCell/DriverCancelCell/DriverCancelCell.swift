//
//  DriverCancelCell.swift
//  Muber
//
//  Created by Jaydeep on 30/03/18.
//

import UIKit

class DriverCancelCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblCancelName: UILabel!
    @IBOutlet weak var imgOfCollapse: UIImageView!
    @IBOutlet weak var lblCancelHelp: UILabel!
    @IBOutlet weak var lblCancelDesc: UILabel!
    @IBOutlet weak var btnNoOutlet: CustomButton!
    @IBOutlet weak var btnYesOutlet: CustomButton!
    @IBOutlet weak var viewCancelHelp: UIView!
    @IBOutlet weak var viewOfDesc: UIView!
    @IBOutlet weak var viewOfAgree: UIView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    @IBOutlet weak var viewOfBottom: UIView!
    

    //MARK:- ViewLife Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        [btnYesOutlet,btnNoOutlet].forEach { (btn) in
            btn?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        }
        
        btnNoOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        btnNoOutlet.backgroundColor = .clear
        btnNoOutlet.setTitleColor(MySingleton.sharedManager.themeLightTextColor, for: .normal)
        btnNoOutlet.setTitle(mapping.string(forKey: "NO_key"), for: .normal)
        
        btnYesOutlet.titleLabel?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        btnYesOutlet.backgroundColor = MySingleton.sharedManager.themeRedColor
        btnYesOutlet.setTitleColor(.white, for: .normal)
        btnYesOutlet.setTitle(mapping.string(forKey: "YES_key"), for: .normal)
         
        lblCancelName?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 19)
        lblCancelName?.textColor = MySingleton.sharedManager.themeLightTextColor
        
        lblCancelHelp?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 20)
        lblCancelHelp?.textColor = .black
        lblCancelHelp.text = mapping.string(forKey: "CANCEL_HELP_key")
        
        lblCancelDesc?.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 15)
        lblCancelDesc?.textColor = MySingleton.sharedManager.themeLightTextColor
        lblCancelDesc.text = mapping.string(forKey: "We_will_recharge_20%_of_the_requested_service_key")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
