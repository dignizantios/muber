//
//  VehicleTypeCell.swift
//  Muber
//
//  Created by Jaydeep on 23/03/18.
//

import UIKit

class VehicleTypeCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgOfVehicleType: UIImageView!
    @IBOutlet weak var lblVehicleType: UILabel!
    

    //MARK:- View LifeCycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.lblVehicleType.font = MySingleton.sharedManager.getFontForTypeBoldWithSize(fontSize: 17)
        self.lblVehicleType.textColor = UIColor.black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
