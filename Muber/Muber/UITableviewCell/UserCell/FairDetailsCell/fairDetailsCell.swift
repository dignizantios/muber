//
//  fairDetailsCell.swift
//  Muber
//
//  Created by YASH on 03/04/18.
//

import UIKit

class fairDetailsCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblValue: UILabel!
    
    
    //MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 17)
        lblTitle.textColor = .black
        
        lblValue.font = MySingleton.sharedManager.getFontForTypeSemiBoldWithSize(fontSize: 17)
        lblValue.textColor = MySingleton.sharedManager.themeLightTextColor
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
