//
//  HistoryCell.swift
//  Muber
//
//  Created by Jaydeep on 23/03/18.
//

import UIKit

class HistoryCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    //MARK:- ViewLife cycle
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.lblTime.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
        self.lblTime.textColor = .white
        
        self.lblPrice.font = MySingleton.sharedManager.getFontForTypeBoldWithSize(fontSize: 20)
        self.lblPrice.textColor = MySingleton.sharedManager.themeRedColor
        
        self.lblType.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 15)
        self.lblType.textColor = MySingleton.sharedManager.themeLightTextColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
