//
//  UIViewController+Extension.swift
//  Muber
//
//  Created by Jaydeep on 22/03/18.
//

import Foundation
import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

extension UIViewController
{
    //MARK: - Print All Fonts
    
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    //MARK: - SetUpNaviagtionBar
    
    func setupNavigationBarWithSideMenu(titleText:String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeNaviColor
        
        if self.revealViewController() == nil {
            print("Side Menu nil")
        }
        let revealController: SWRevealViewController? = revealViewController()
        revealController?.panGestureRecognizer().isEnabled = true
        revealController?.tapGestureRecognizer().isEnabled = true
        
        let leftImage  = UIImage(named: "ic_menubar_header")!.withRenderingMode(.alwaysOriginal)
        var leftButton = UIButton(frame: CGRect(x:0,y: 0,width: 20,height: 44))
        leftButton = UIButton(type: .custom)       
        leftButton.setBackgroundImage(leftImage, for: .normal)
        leftButton.addTarget(revealController, action: #selector(revealController?.revealToggle(_:)), for:.touchUpInside)
        
        let menuButton = UIBarButtonItem(customView: leftButton)
        self.navigationItem.leftBarButtonItem = menuButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 40, height: 19))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
        HeaderView.textColor = UIColor.white
        self.navigationItem.titleView = HeaderView
        
        self.navigationItem.hidesBackButton = true
        
    }
    
    
    func setupNavigationBarWithBackButton(titleText:String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeNaviColor
        
        if self.revealViewController() == nil {
            print("Side Menu nil")
        }
        let revealController: SWRevealViewController? = revealViewController()
        revealController?.panGestureRecognizer().isEnabled = true
        revealController?.tapGestureRecognizer().isEnabled = true
        
        let leftImage  = UIImage(named: "ic_back_arrow_white_header")!.withRenderingMode(.alwaysOriginal)
        var leftButton = UIButton(frame: CGRect(x:0,y: 0,width: 20,height: 44))
        leftButton = UIButton(type: .custom)
        leftButton.tintColor = .white
        leftButton.setBackgroundImage(leftImage, for: .normal)
        leftButton.addTarget(self, action: #selector(backButtonAction), for:.touchUpInside)
        
        let menuButton = UIBarButtonItem(customView: leftButton)
        self.navigationItem.leftBarButtonItem = menuButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 40, height: 19))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.font = MySingleton.sharedManager.getFontForTypeRegularWithSize(fontSize: 19)
        HeaderView.textColor = UIColor.white
        self.navigationItem.titleView = HeaderView
        
        self.navigationItem.hidesBackButton = true
        
    }
    
    @objc func backButtonAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Attributed Convert
    
    func attributedString(string1:String,FontColor:UIColor,Font:UIFont) -> NSAttributedString?
    {
        let attributes2 = [
            NSAttributedStringKey.font : Font,
            NSAttributedStringKey.foregroundColor : FontColor,
            NSAttributedStringKey(rawValue: NSAttributedStringKey.underlineStyle.rawValue) : NSUnderlineStyle.styleSingle.rawValue
            ] as [NSAttributedStringKey : Any]
        
        let attributedString2 = NSAttributedString(string: " \(string1)", attributes: attributes2)
        
        let FormatedString = NSMutableAttributedString()
        FormatedString.append(attributedString2)
        
        return FormatedString
    }
    
    //MARK:- Email Validataion
    
    func isValidEmail(emailAddressString:String) -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    
    //MARK: - Add Done button
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = MySingleton.sharedManager.themeRedColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.view.endEditing(true)
    }
    
    //MARK:- Google Maps setup
    
    func setupGoogleMaps(view:GMSMapView)  {
        do {
            if let styleURL = Bundle.main.url(forResource: "GoogleMapsStyle", withExtension: "json") {
                view.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    //MARK: - Image Choose Methods
    
    func ShowChooseImageOptions(picker : UIImagePickerController)
    {
        let alertController = UIAlertController(title: "", message: mapping.string(forKey: "Choose_Option_key"), preferredStyle: .actionSheet)
        
        let cameraButton = UIAlertAction(title: mapping.string(forKey: "Take_a_photo_key"), style: .default, handler: { (action) -> Void in
            print("camera button tapped")
            self.openCamera(picker : picker)
        })
        
        let  galleryButton = UIAlertAction(title: mapping.string(forKey: "Select_from_Photos_key"), style: .default, handler: { (action) -> Void in
            
            print("gallery button tapped")
            self.openGallary(picker : picker)
        })
        let  dismissButton = UIAlertAction(title: mapping.string(forKey: "Cancel_key"), style: .cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(cameraButton)
        alertController.addAction(galleryButton)
        alertController.addAction(dismissButton)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    func openGallary(picker : UIImagePickerController)
    {
        //  picker.allowsEditing = true
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    
    func openCamera(picker : UIImagePickerController)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            //  picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title:"" , message: "Device not support to take photos", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- userdefault 
    
    func getUserDetail(_ forKey: String) -> String
    {
        guard let userDetail = UserDefaults.standard.value(forKey: "userDetail") as? Data else { return "" }
        let data = JSON(userDetail)
        //print(data)
        return data[forKey].stringValue
    }
    
}
