//
//  ServicePrefixList.swift
//  Muber
//
//  Created by Jaydeep on 14/04/18.
//

import Foundation

var MuberURL:String = "http://dignizant.com/muber/api/user/"

var userRegister = "register"
var userTermCondition = "terms_condition"
var userVerifyOTP = "verify_by_otp"
var userLogin = "login"
var userForgotPassword = "forgot_password"
var userRegiVerifyOTP = "otp_check"
var userResendOTP = "resend_otp"
var userChangePassword = "change_password"
