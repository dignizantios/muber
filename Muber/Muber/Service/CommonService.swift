//
//  CommonService.swift
//  Muber
//
//  Created by Jaydeep on 14/04/18.
//

import Foundation
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

struct CommonService
{
    func Service(url:String,param : [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        print("url - ",url)
        
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).authenticate(user: BasicUsername, password:BasicPassword).responseSwiftyJSON(completionHandler:
            {
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    print("StatusCode : \(statusCode)")
                    if(statusCode == 500)
                    {
                        
                    }else if(statusCode != nil)
                    {
                        /// APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }
                    else
                    {
                        toastMessage(title: mapping.string(forKey: "Something_went_wrong_key"))                        
                        completion($0.result)
                    }
                }else
                {
                    toastMessage(title: mapping.string(forKey: "Something_went_wrong_key"))
                }
        })
    }
    
}
